# Tutorium Programmieren

This project contains all solutions and code examles of my lectures during my time at the Techische Universität Braunschweig.
I worked there as working student between Oct. 2016 and Mar. 2021.

*Please be aware that all slides are in **german**!*

## Overview

### Beginner part is called *prog1* (Programming course 1)

Slides are available at [/slides/prog1](https://gitlab.com/dev_rohde1997/tutorium/-/tree/master/slides/prog1)

Code is available at [/src/vorkurs](https://gitlab.com/dev_rohde1997/tutorium/-/tree/master/src/prog1)

### Advanced part is called *prog2* (Programming course 2) 

Slides are available at [/slides/prog2](https://gitlab.com/dev_rohde1997/tutorium/-/tree/master/slides/prog2)

Code is available at [/src/vorkurs](https://gitlab.com/dev_rohde1997/tutorium/-/tree/master/src/prog2)

### Crash course programming 

Code is available at [/src/vorkurs](https://gitlab.com/dev_rohde1997/tutorium/-/tree/master/src/vorkurs)


