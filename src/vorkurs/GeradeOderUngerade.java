package vorkurs;

import java.util.Scanner;

public class GeradeOderUngerade {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Zahl einlesen
        System.out.print("Bitte eine Zahl zwischen 2 und 100 eingeben:");
        int eingabe = scanner.nextInt();

        if (eingabe > 2 & eingabe < 100) {

        } else {
            System.out.println("Zahl nicht liegt im gültigen Bereich");
        }

        int rest = eingabe % 2;
        //Wenn rest == 0 -> eingabe war gerade
        //Wenn rest == 1 -> eingabe war ungerade

        switch (rest) {
            case 0:
                System.out.println("Die Zahl (" + eingabe + ") ist gerade");
                break;
            case 1:
                System.out.println("Die Zahl(" + eingabe + ") ist ungerade");
                break;
            default:
                System.out.println("Unbekannter Rest");
                break;
        }


    }
}

