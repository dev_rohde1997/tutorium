package vorkurs;

import java.io.*;
import java.net.Socket;
import java.net.URLEncoder;

public class Client {
    private String session_name;

    public Client(String session_name) {
        this.session_name = session_name;
    }

    public static void main(String[] args) {
        Client client = new Client("my_game");
        String field = "";

        try {
            //DATA
            String username = "Moritz";
            String username2 = "Kevin";

            client.printNumberField();
            checkMatches(client);

            startGame(client, username, username2);

            /*client.send_turn(session_name, username2, 1);
            client.send_turn(session_name, username, 2);
            client.send_turn(session_name, username2, 3);
            client.send_turn(session_name, username, 4);
            client.send_turn(session_name, username2, 5);
            client.send_turn(session_name, username, 6);
            client.send_turn(session_name, username2, 7);
            client.send_turn(session_name, username, 8);
            client.send_turn(session_name, username2, 9);*/
           // client.send_turn(session_name, username, 9);

            field = client.getField();
            System.out.println(field);


            //client.deleteSession(session_name, username);


        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private static void startGame(Client client, String username, String username2) throws IOException {
        //NEW GAME
        System.out.println("->new");
        client.new_game(username);
        checkMatches(client);


        //Connect
        System.out.println("->connect");
        client.connect_to_session(username2);
        checkMatches(client);
    }

    private static void checkMatches(Client client) throws IOException {
        //Check matches
        //System.out.println("/matches");
        client.get("/matches");
    }

    public void get(String route) throws IOException {
        String ip = "127.0.0.1"; // localhost
        int port = 8000;
        java.net.Socket socket = new java.net.Socket(ip,port); // verbindet sich mit Server

        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        // send an HTTP request to the web server
        //out.println("GET / HTTP/1.1");
        out.println("GET " + route + " HTTP/1.1");
        out.println("Host: localhost:8000");
        out.println("Connection: Close");
        out.println();

        // read the response
        boolean loop = true;
        StringBuilder sb = new StringBuilder(8096);
        while (loop) {
            if (in.ready()) {
                int i = 0;
                while (i != -1) {
                    i = in.read();
                    sb.append((char) i);
                }
                loop = false;
            }
        }
       // System.out.println(sb.toString());
        String[] output_parts = sb.toString().split("\n");

        System.out.println(output_parts[output_parts.length-1]);
        socket.close();
    }

    public void post(String message, String path) throws IOException {
        String data = (URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(message, "UTF-8"));

        post_IO(path, data);
    }

    public void connect_to_session(String username) throws IOException {
        String data = "";
        data += (URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&");
        data +=(URLEncoder.encode("session_name", "UTF-8") + "=" + URLEncoder.encode(session_name, "UTF-8"));
        post_IO("/connect", data);
    }

    public void deleteSession(String username) throws IOException {
        String data = "";
        data += (URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&");
        data +=(URLEncoder.encode("session_name", "UTF-8") + "=" + URLEncoder.encode(session_name, "UTF-8"));
        post_IO("/deleteGame", data);
    }

    public String getField() throws IOException {
        String data = "";
        data +=(URLEncoder.encode("session_name", "UTF-8") + "=" + URLEncoder.encode(session_name, "UTF-8"));
        String reply = post_IO("/getField", data);
        String temp = "";
        reply = reply.replace("[", "")
                .replace("]", "")
                .replace("\"", "")
                .replace(",", ", ");

        return reply;
    }

    public String send_turn(String username, int pos) throws IOException {
        if (pos < 1 || pos > 9) {
            System.out.println("Positionen nur von 1 bis 9 erlaubt! ->" + pos);
            printNumberField();
            return session_name;
        }

        //Send turn
        String data = "";
        data += (URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&");
        data += (URLEncoder.encode("turn", "UTF-8") + "=" + URLEncoder.encode("" + pos, "UTF-8") + "&");
        data +=(URLEncoder.encode("session_name", "UTF-8") + "=" + URLEncoder.encode(session_name, "UTF-8"));
        String reply = post_IO("/sendTurn", data);
        //System.out.println(reply);
        return reply;


    }

    public void new_game( String username) throws IOException {
        String data = "";
        data += (URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&");
        data +=(URLEncoder.encode("session_name", "UTF-8") + "=" + URLEncoder.encode(session_name, "UTF-8"));
        String reply = post_IO("/new_game", data);

    }

    private String post_IO(String path, String data) throws IOException {
        Socket socket = new Socket("127.0.0.1", 8000);

        BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
        wr.write("POST " + path + " HTTP/1.0\r\n");
        wr.write("Content-Length: " + data.length() + "\r\n");
        wr.write("Content-Type: application/x-www-form-urlencoded\r\n");
        wr.write("\r\n");
        wr.write(data);

        wr.flush();

        //TODO: Handle Output
        BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;
        String str_data = "";
        while ((line = rd.readLine()) != null) {
            str_data += line + "\n";
        }

        wr.close();
        rd.close();
        socket.close();

        String[] output_parts = str_data.split("\n");
        return output_parts[output_parts.length-1];
    }

    public void printNumberField() {
        System.out.print("\n\t");
        for (int k = 0; k < 8 * 3 - 1; k++)
            System.out.print("-");
        System.out.println();
        //Ausgabe der Matrix
        int l = 1;
        for (int i = 0; i < 3; i++) {
            System.out.print("\t||");
            for (int j = 0; j < 3; j++) {
                System.out.printf("  %d  ||", l + i + j);
            }
            System.out.print("\n\t");
            for (int k = 0; k < 8 * 3 - 1; k++)
                System.out.print("-");
            System.out.println();
            l+=2;
        }
    }

    public String getSession_name() {
        return session_name;
    }

    public void setSession_name(String session_name) {
        this.session_name = session_name;
    }
}

