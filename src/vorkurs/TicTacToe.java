package vorkurs;

import java.io.IOException;

public class TicTacToe {
    private char[][] field = new char[3][3];
    private Client client;
    private String username;

    public static void main(String[] args) {
        TicTacToe ticTacToe_moritz = new TicTacToe("my game", "Moritz");
        TicTacToe ticTacToe_kevin = new TicTacToe("my game", "Kevin");

        try {


            ticTacToe_moritz.newGame();
            ticTacToe_kevin.connect();


            while (!ticTacToe_moritz.checkWin()) {
                System.out.println(ticTacToe_moritz.username);
                ticTacToe_moritz.printField();
                while (ticTacToe_moritz.setField('X', (int) (Math.random() * 9 + 1))) {
                    //Wait
                    System.out.println("Next try");
                }
                System.out.println(ticTacToe_moritz.username);


                ticTacToe_moritz.refreshField();
                ticTacToe_moritz.printField();


                if (ticTacToe_moritz.checkWin())
                    break;

                System.out.println(ticTacToe_kevin.username);
                ticTacToe_kevin.printField();
                while (ticTacToe_kevin.setField('O', (int) (Math.random() * 9 + 1))) {
                    //Wait
                    System.out.println("Next try");
                }
                System.out.println(ticTacToe_kevin.username);

                ticTacToe_kevin.printField();
                ticTacToe_kevin.refreshField();

            }



        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public TicTacToe(String session_name, String username) {
        client = new Client(session_name);
        this.username = username;
        init_field();
    }

    public boolean setField(char c, int pos) throws IOException {
        if (pos < 1 || pos > 9)
            return false;
        String reply = client.send_turn(username, pos);
        System.out.println(reply);
        if (reply.contains("Next turn:")) {

            pos -= 1;
            field[pos / 3][pos % 3] = c;
            return true;
        }
        return false;
    }

    public void printField() {
        System.out.print("\n\t");
        for (int k = 0; k < 8 * field.length - 1; k++)
            System.out.print("-");
        System.out.println();
        for (int i = 0; i < field.length; i++) {
            System.out.print("\t||");
            for (int j = 0; j < field.length; j++) {
                System.out.printf("  %c  ||", field[i][j]);
            }
            System.out.print("\n\t");
            for (int k = 0; k < 8 * field.length - 1; k++)
                System.out.print("-");
            System.out.println();
        }
    }

    public void newGame() throws IOException {
        client.new_game(username);
    }

    public void connect() throws IOException {
        client.connect_to_session(username);
    }

    public void changeSession(String name) {
        this.client.setSession_name(name);
    }

    private void refreshField() throws IOException {
        String str_field = " " + client.getField();
        String[] field_parts = str_field.split(",");
        for (int i = 0; i < field_parts.length; i++) {
            switch (field_parts[i].length()) {
                case 0:
                case 1:
                    field[i / 3][i % 3] = '_';
                    break;
                case 2:
                    field[i / 3][i % 3] = field_parts[i].charAt(1);
                    break;
            }
        }
        printField();
    }


    private boolean checkWin() {
        if (checkHoritonzal('X') || checkVertical('X') || checkDiagonal('X')) {
            System.out.println("X won");
            return true;
        }
        if (checkHoritonzal('O') || checkVertical('O') || checkDiagonal('O')) {
            System.out.println("O won");
            return true;
        }
        if (draw()) return true;
        return false;
    }

    private boolean checkHoritonzal(char c) {
        for (int i = 0; i < field.length; i++) {
            if (field[i][0] == c && field[i][1] == c && field[i][2] == c)
                return true;
        }
        return false;
    }

    private boolean checkVertical(char c) {
        for (int i = 0; i < field.length; i++) {
            if (field[0][i] == c && field[1][i] == c && field[2][i] == c)
                return true;
        }
        return false;
    }

    private boolean checkDiagonal(char c) {
        if (field[0][0] == c && field[1][1] == c && field[2][2] == c) return true;
        if (field[0][2] == c && field[1][1] == c && field[2][0] == c) return true;
        return false;
    }

    private boolean draw() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == '_') return false;
            }
        }
        System.out.println("DRAW");
        return true;
    }

    private void init_field() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = '_';
            }
        }
    }

}
