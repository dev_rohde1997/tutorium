package vorkurs;

public class Caesar {
    public static void main(String[] args) {

        String text = "Das ist ein super geheimer Text und den darf niemand lesen";

        System.out.println("Original: " + text);

        String encrypted = encrypt(text);
        System.out.println("Verschl: " + encrypted);

        System.out.println("Entschl: " + decrypt(encrypted));

    }

    public static String encrypt(String word) {
        String out = "";
        for (char c : word.toCharArray()) {
            int char_als_int = (int) c;
            int moves = 0;
            while (moves < 4) {

                if (char_als_int == 90) {
                    char_als_int = 97;
                    moves++;
                    continue;
                }

                if (char_als_int == 122) {
                    char_als_int = 65;
                    moves++;
                    continue;
                }

                char_als_int++;
                moves++;
            }

            out += (char) char_als_int;
        }

        return out;
    }

    public static String decrypt(String word) {
        StringBuilder out = new StringBuilder();
        for (char c : word.toCharArray()) {
            int char_as_int = (int) c;
            int moves = 0;
            while (moves < 4) {
                if (char_as_int == 97) {
                    char_as_int = 90;
                    moves++;
                    continue;
                }

                if (char_as_int == 65) {
                    char_as_int = 122;
                    moves++;
                    continue;
                }

                char_as_int--;
                moves++;
            }
            out.append((char) char_as_int);
        }
        return out.toString();
    }
}
