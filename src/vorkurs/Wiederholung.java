package vorkurs;

public class Wiederholung {
    public static void main(String[] args) {

        //Ganzzahlen
        byte b = 8; // 0000 0000
        short s = 8; // 2 Byte - 16 Bit
        int i = 42; // 4 Byte - 32 Bit
        long l = 42; // 8 Byte - 64 Bit

        //Flieskommazahlen
        double d = 42.0;
        float f = 23.6f;

        //Text
        String string = "";


        //Zeichen
        char c = ' ';

        //wahrheitswerte
        boolean b1 = true;
        boolean b2 = false;

        // if Anweiungen

        if (b1) {
            System.out.println("IF");
        } else if (b2){
            System.out.println("ELSE IF");
        } else {
            System.out.println("ELSE");
        }

        //char, String, int, double, KEIN Boolean
        switch (c) {
            case 'A':
                System.out.println("das ist ein a");
                break;
            default:
                System.out.println("Nichts passendes gefunden");
                break;
        }

        //Schleifen

        //while

        int counter = 0;
        while (b1) {
            //Anweisungen
            System.out.println("Ich bin in der Schleife");
            counter = counter + 1;
            counter++;
            counter += 1;

            //Abbruchbedingung
            if (counter > 10) {
                b1 = false;
            }
        }

        //for - counter schleife
        for (int j = 0; j < 10; j++) {
            System.out.println(j + " | ");

            //break bricht schleife ab
            if (j % 4 == 0) {
                break;
            }

            //continue springt noch oben
            if (j % 3 == 0) {
                continue;
            }

            System.out.println("Meine For schleife");
        }

        //Listen
        //bzw Arrays
        int[] liste = new int[10];

        //Arrays gleich mit Werten anlegen
        int[] zahlen_liste = {1,2,3,4,5,6,7,8,9};

        //for each schleife
        for(int zahl: zahlen_liste) {
            //Ausgeben aller Zahlen aus der Liste
            System.out.println("Meine Zahl: " + zahl);
        }

        //Casten
        //Typumwandlung

        //Primitiven Datentypen

        byte mein_byte = (byte) 123456;
        short mein_short = (short) 12345678;
        int mein_int = (int) 123456789L;
        long mein_long = 123456789;

        //Char casten
        char mein_char = 65;
        int mein_char_als_int = (int) mein_char;

        //String und Zahlen umwandeln
        String text = "123";
        // String -> int
        int text_als_int = Integer.parseInt(text);

        //int -> String
        //Ordentlich
        text = String.valueOf(text_als_int);

        //Dreckig
        text = "" + text_als_int;



        //Statische Methoden - Aufrufen
        name_der_methode(1, 2);
        int rueckgabe_wert = name_der_methode_2(4);

    }

    //Statische Methoden - Deklarieren

    //Ohne Rückgabewert
    static void name_der_methode(int parameter1, int parameter2) {
        //Anweisungen
        System.out.println("Ende der Methode");
    }

    //Mit Rückgabewert
    static int name_der_methode_2(int parameter1) {
        return -1;
    }



}
