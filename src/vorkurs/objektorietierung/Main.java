package vorkurs.objektorietierung;

public class Main {
    public static void main(String[] args) {
        Datum datum = new Datum(8, 7, 2019);
        Datum datum2 = new Datum(9, 7, 2019);
        Termin termin = new Termin("Uni", datum);
        Termin termin2 = new Termin("Arbeit", datum2);
        System.out.println(datum);
        System.out.println(termin);

        if (termin.ist_spaeter(termin2)) {
            System.out.println(termin.getDatum() + " ist spaeter");
        } else {
            System.out.println(termin2.getDatum() + " ist spaeter");

        }
    }
}
