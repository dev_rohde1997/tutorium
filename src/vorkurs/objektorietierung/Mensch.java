package vorkurs.objektorietierung;

public class Mensch {
    String name;
    String augenfarbe = "Blau";
    boolean ist_lebendig = true;
    double schuhgroesse = 17.5;
    int koerpergroesse = 49; //in cm

    Mensch(String name) {
        this.name = name;
    }

    void sterben() {
        ist_lebendig = false;
    }

    static void wasBinIch() {
        System.out.println("Ich bin ein Mensch");
    }

    public String toString() {
        String output = String.format("Mein Name ist %s mit der Augenfarbe %s.%n" +
                "Ich habe die Schuhgroesse %.1f und bin %dcm gross.%n",
                name, augenfarbe, schuhgroesse, koerpergroesse);

        if (ist_lebendig) {
            output += "Ich lebe!\n";
        } else {
            output += "Ich bin leider nicht mehr so lebendig.\n";
        }

        return output;
    }
}
