package vorkurs.objektorietierung;

public class Student {
    static String status = "Student";
    String name;
    String studiengang;
    int matrikelnummer;
    int semester = 1;

    Student(String name, String studiengang) {
        this.name = name;
        this.studiengang = studiengang;
        matrikelnummer = berechneMatrikelnummer();
    }

    private int berechneMatrikelnummer() {
        int zahl =  (int) (Math.random() * 9999999);
        while (zahl < 1000000) {
            zahl =  (int) (Math.random() * 9999999);
        }
        return zahl;
    }

    public String toString() {
        String output = String.format("Ich heisse %s.%nIch studiere %s im %d." +
                "Semester. Meine Matrikelnummer ist %d", name, studiengang, semester, matrikelnummer);

        return output;
    }
}
