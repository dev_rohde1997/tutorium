package vorkurs.objektorietierung;

public class Menschen_und_Geburtstage {
    public static void main(String[] args) {
        Geburtstag geburtstag = new Geburtstag(17, 9, 1997);
        Menschelein menschelein = new Menschelein("Moritz", geburtstag);
        System.out.println(menschelein);
    }
}


class Menschelein {
    private String name;
    private Geburtstag geburtstag;

    public Menschelein(String name, Geburtstag geburtstag) {
        this.name = name;
        this.geburtstag = geburtstag;
    }

    @Override
    public String toString() {
        return "" + name + " hat Geburtstag am " + geburtstag;
    }
}

class Geburtstag {
    private int tag;
    private int monat;
    private int jahr;

    public Geburtstag(int tag, int monat, int jahr) {
        this.tag = tag;
        this.monat = monat;
        this.jahr = jahr;
    }

    @Override
    public String toString() {
        String output = "";
        if (tag < 10) {
            output += "0";
        }
        output += tag;

        output += "/";

        if (monat < 10) {
            output += "0";
        }
        output += monat;
        output += "/";
        output += jahr;

        return output;
    }
}