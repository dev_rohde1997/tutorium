package vorkurs.objektorietierung;

public class Bankaccount {
    private int id;
    private int passwort;


    Bankaccount(int id, int passwort) {
        this.id = id;
        this.passwort = passwort;
    }



    public int getId() {
        return id;
    }

    public int getPasswort() {
        System.out.println("ACHTUNG!!!\nPasswort abgerufen!");

        return passwort;
    }

    public void setPasswort(int passwort, int sicherheitscode) {
        if(checkeSicherheitscode(sicherheitscode)) {
            System.out.println("ACHTUNG!!!\nPasswort wurde geändert!");
            this.passwort = passwort;
        }

    }

    private boolean checkeSicherheitscode(int sicherheitscode) {
        //Überprüfung
        return true;
    }
}

