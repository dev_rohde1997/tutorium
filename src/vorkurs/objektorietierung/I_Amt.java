package vorkurs.objektorietierung;

public class I_Amt {
    public static void main(String[] args) {
        Student student_1 = new Student("Moritz", "Wirtschaftsinformatik");
        Student student_2 = new Student("Kevin", "Informatik");

        System.out.println(student_1);
        System.out.println(student_2);
    }
}
