package vorkurs.objektorietierung;

public class Krankenhaus {
    public static void main(String[] args) {
        Mensch mensch_1 = new Mensch("Moritz");
        Mensch mensch_2 = new Mensch("Peter");
        Mensch mensch_3 = new Mensch("Max Mustermann");

        //Ändern von Eigenschaften
        mensch_1.schuhgroesse = 45;
        mensch_1.koerpergroesse = 179;

        /*System.out.println("Ausgabe von " + mensch_1.name);
        System.out.println(mensch_1);

        System.out.println("Ausgabe von " + mensch_2.name);
        System.out.println(mensch_2);

        mensch_3.sterben();
        System.out.println("Ausgabe von " + mensch_3.name);
        System.out.println(mensch_3);*/

        //Zufallszahl
        System.out.println((int) (Math.random() * 100));
    }
}
