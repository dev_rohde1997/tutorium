package vorkurs.objektorietierung;

public class Termin {
    private String titel = "";
    private Datum datum;

    Termin(String titel, Datum datum) {
        this.titel = titel;
        this.datum = datum;
    }

    @Override
    public String toString() {
        return "" + titel + " ist am: " + datum;
    }

    public boolean ist_spaeter(Termin t) {
        return datum.ist_spaeter(t.datum);
    }

    public Datum getDatum() {
        return datum;
    }
}


class Datum {
    int tag;
    int monat;
    int jahr;

    public Datum(int tag, int monat, int jahr) {
        this.tag = tag;
        this.monat = monat;
        this.jahr = jahr;
    }

    @Override
    public String toString() {
        return "" + tag + "/" + monat + "/" + jahr;
    }

    public boolean ist_spaeter(Datum datum) {
        if (jahr == datum.jahr) {
            if (monat == datum.monat) {
                if (tag == datum.tag) {
                    return false;
                }
                if (tag < datum.tag) {
                    return false;
                } else {
                    return true;
                }
            }
            if (monat < datum.monat) {
                return false;
            } else {
                return true;
            }
        }
        if (jahr < datum.jahr) {
            return false;
        } else {
            return true;
        }


    }
}