package vorkurs;

import java.util.Scanner;

public class Rechner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int zahl1 = scanner.nextInt();
        int zahl2 = scanner.nextInt();

        //Methoden testen
        System.out.println(zahl1 + " + " + zahl2 + " = " + addieren(zahl1, zahl2));
        System.out.println(zahl1 + " - " + zahl2 + " = " + subtrahieren(zahl1, zahl2));
        System.out.println(zahl1 + " * " + zahl2 + " = " + multiplizieren(zahl1, zahl2));
        System.out.println(zahl1 + " / " + zahl2 + " = " + dividieren(zahl1, zahl2));
    }


    static int addieren(int zahl, int zahl2) {
        int erg = zahl + zahl2;
        return erg;
    }

    static int subtrahieren(int zahl, int zahl2) {
        return zahl - zahl2;
    }

    static int multiplizieren(int zahl, int zahl2) {
        return zahl * zahl2;
    }

    static int dividieren(int zahl, int zahl2) {
        if (zahl2 == 0) {
            System.out.println("Man darf nicht durf 0 teilen!!");
            return -1;
        }
        return zahl / zahl2;

    }
}
