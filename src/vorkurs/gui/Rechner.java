package vorkurs.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Rechner {
    public static void main(String[] args) {

        //Variablen
        JLabel result_label = new JLabel();



        JFrame jFrame = new JFrame();
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(300, 300);
        jFrame.setBackground(Color.BLUE);
        jFrame.setLayout(new BorderLayout());

        //Top Layout
        JPanel top_panel = new JPanel();
        top_panel.setLayout(new FlowLayout());

        //Label
        JLabel label_1 = new JLabel();
        label_1.setText("Zahl 1:");

        JTextField tf_1 = new JTextField();
        tf_1.setPreferredSize(new Dimension(30, 20));

        JLabel label_2 = new JLabel();
        label_2.setText("Zahl 2:");

        JTextField tf_2 = new JTextField();
        tf_2.setPreferredSize(new Dimension(30, 20));


        top_panel.add(label_1);
        top_panel.add(tf_1);
        top_panel.add(label_2);
        top_panel.add(tf_2);


        //Middle Layout

        JPanel middle_panel = new JPanel();
        middle_panel.setLayout(new GridLayout(2, 2));

        //Buttons
        JButton jButton_plus = new JButton();
        jButton_plus.setText("+");
        jButton_plus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double zahl1 = Double.parseDouble(tf_1.getText());
                double zahl2 = Double.parseDouble(tf_2.getText());

                double ergebnis = zahl1 + zahl2;

                result_label.setText(""+ergebnis);

            }
        });


        JButton jButton_minus = new JButton();
        jButton_minus.setText("-");
        jButton_minus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double zahl1 = Double.parseDouble(tf_1.getText());
                double zahl2 = Double.parseDouble(tf_2.getText());

                double ergebnis = zahl1 - zahl2;

                result_label.setText(""+ergebnis);
            }
        });

        JButton jButton_muliply = new JButton();
        jButton_muliply.setText("*");
        jButton_muliply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double zahl1 = Double.parseDouble(tf_1.getText());
                double zahl2 = Double.parseDouble(tf_2.getText());

                double ergebnis = zahl1 * zahl2;

                result_label.setText(""+ergebnis);
            }
        });

        JButton jButton_divide = new JButton();
        jButton_divide.setText("/");
        jButton_divide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double zahl1 = Double.parseDouble(tf_1.getText());
                double zahl2 = Double.parseDouble(tf_2.getText());

                if (zahl2 == 0) {
                    result_label.setText("Man darf nicht durch 0 teilen.");
                } else {
                    double ergebnis = zahl1 / zahl2;
                    result_label.setText(""+ergebnis);
                }

            }
        });

        middle_panel.add(jButton_plus);
        middle_panel.add(jButton_minus);
        middle_panel.add(jButton_muliply);
        middle_panel.add(jButton_divide);




        //Botton Layout

        JPanel bottom_panel = new JPanel();
        bottom_panel.setLayout(new FlowLayout());

        JLabel result_label_text = new JLabel();
        result_label_text.setText("Ergebnis: ");

        bottom_panel.add(result_label_text);
        bottom_panel.add(result_label);

        //Adding Components
        jFrame.add(top_panel, BorderLayout.NORTH);
        jFrame.add(middle_panel, BorderLayout.CENTER);
        jFrame.add(bottom_panel, BorderLayout.SOUTH);

        jFrame.setVisible(true);






    }
}
