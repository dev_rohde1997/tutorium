package vorkurs.gui;

import vorkurs.Caesar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CaesarGUI {
    public static void main(String[] args) {

        JFrame jFrame = new JFrame();
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setPreferredSize(new Dimension(500, 500));
        jFrame.setSize(800, 800);
        jFrame.setLocation(500, 500);
        jFrame.setLayout(new BorderLayout());

        JTextArea input = new JTextArea();
        input.setFont(new Font("mFont", Font.PLAIN, 25));
        input.setBorder(BorderFactory.createLineBorder(Color.black, 2, true));

        jFrame.add(input, BorderLayout.CENTER);

        JLabel headline = new JLabel();
        headline.setText("Mein Verschlüsselungsprogramm");
        headline.setFont(new Font("MyFont", Font.PLAIN, 35));
        headline.setHorizontalAlignment(SwingConstants.CENTER);
        jFrame.add(headline, BorderLayout.NORTH);

        FlowLayout flowLayout = new FlowLayout();
        Container container = new Container();
        container.setLayout(flowLayout);

        //ENCRYPT
        JButton btn_encrypt = new JButton();
        btn_encrypt.setText("Verschlüsseln");
        btn_encrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String new_word = Caesar.encrypt(input.getText());
                input.setText(new_word);
            }
        });

        //DECRYPT
        JButton btn_decrypt = new JButton();
        btn_decrypt.setText("Entschlüsseln");
        btn_decrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String new_word = Caesar.decrypt(input.getText());
                input.setText(new_word);
            }
        });

        container.add(btn_encrypt);
        container.add(btn_decrypt);

        jFrame.add(container, BorderLayout.SOUTH);

        jFrame.validate();
        jFrame.repaint();

    }
}
