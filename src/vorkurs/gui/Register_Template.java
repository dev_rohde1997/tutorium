package vorkurs.gui;

import javax.swing.*;
import java.awt.*;

public class Register_Template {
    public static void main(String[] args) {


        String[] eingaben = new String[]{"Vorname", "Nachname", "Username", "Geschlecht", "Adresse", "Wohnort", "PLZ", "Geburtstag"};

        JLabel[] jLabel_list = new JLabel[eingaben.length];
        JTextField[] jTF_list = new JTextField[eingaben.length];


        JFrame jFrame = new JFrame();
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(300, 300);
        jFrame.setLayout(new GridLayout(5, 1));

        for (int i = 0; i < eingaben.length; i++) {
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new FlowLayout());

            JLabel jLabel = new JLabel();
            jLabel.setText(eingaben[i]);

            jLabel_list[i] = jLabel;

            jPanel.add(jLabel);

            JTextField jTextField = new JTextField();
            jTextField.setPreferredSize(new Dimension(150, 30));
            jPanel.add(jTextField);

            jFrame.add(jPanel);
        }

        jFrame.setVisible(true);
    }
}
