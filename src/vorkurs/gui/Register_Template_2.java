package vorkurs.gui;

import org.jetbrains.annotations.NotNull;
import vorkurs.StyleCheck;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Register_Template_2 {
    public static void main(String[] args) {
        String[] eingaben = new String[]{"Vorname", "Nachname", "Username", "Geschlecht", "Adresse", "Wohnort", "PLZ", "Geburtstag"};

        JLabel[] jLabel_list = new JLabel[eingaben.length];
        JTextField[] jTF_list = new JTextField[eingaben.length];
        JButton[] jBtn_list = new JButton[eingaben.length];

        JFrame jFrame = initFrame();

        JPanel headline_panel = buildHeadlinePanel();
        JPanel center_panel = buildCenterFields(eingaben, jLabel_list, jTF_list, jBtn_list);
        JPanel bottom_panel = buildBottomFields(eingaben, jTF_list, jBtn_list);

        jFrame.add(headline_panel, BorderLayout.NORTH);
        jFrame.add(center_panel, BorderLayout.CENTER);
        jFrame.add(bottom_panel, BorderLayout.SOUTH);

        jFrame.validate();
        jFrame.repaint();
    }



    @NotNull
    private static JPanel buildHeadlinePanel() {
        JPanel headline_panel = new JPanel();
        headline_panel.setLayout(new FlowLayout());

        JLabel jL_headline = new JLabel("Daten eingeben");
        headline_panel.add(jL_headline);
        return headline_panel;
    }

    @NotNull
    private static JPanel buildBottomFields(String[] eingaben, JTextField[] jTF_list, JButton[] button_list) {

        JPanel bottom_panel = new JPanel();
        bottom_panel.setLayout(new BorderLayout());
        JPanel bottom_top_panel = new JPanel();

        bottom_top_panel.setLayout(new FlowLayout());


        JLabel jL_output = new JLabel();
        jL_output.setText("OUT");

        bottom_top_panel.add(jL_output);


        //Unten unten
        JPanel bottom_bottom_panel = new JPanel();
        bottom_bottom_panel.setLayout(new FlowLayout());
        JButton btn_send = new JButton("Absenden");
        btn_send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String out = "";

                boolean valid_data = true;



                for (int i = 0; i < jTF_list.length; i++) {
                    if (jTF_list[i].getText().equals("")) {
                        jTF_list[i].setBackground(Color.red);
                        jTF_list[i].setText("Required!");
                        valid_data = false;
                    } else {
                        jTF_list[i].setBackground(Color.white);
                    }
                }

                //Check
                for (int i = 0; i < button_list.length; i++) {
                    button_list[i].doClick();
                    if (jTF_list[i].getBackground().equals(Color.red)) {
                        valid_data = false;
                    }
                }

                if (valid_data) {
                    jL_output.setText("Daten erfolgreich gesendet.");
                } else {
                    jL_output.setText("Fehler beim Senden der Daten!");
                }
            }
        });

        bottom_bottom_panel.add(btn_send);

        bottom_panel.add(bottom_top_panel, BorderLayout.NORTH);
        bottom_panel.add(bottom_bottom_panel, BorderLayout.SOUTH);

        return bottom_panel;
    }

    @NotNull
    private static JPanel buildCenterFields(String[] eingaben, JLabel[] jLabel_list, JTextField[] jTF_list, JButton[] button_list) {
        JPanel center_panel = new JPanel();
        center_panel.setLayout(new GridLayout(5, 1));

        for (int i = 0; i < eingaben.length; i++) {
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new FlowLayout());

            JLabel jLabel = new JLabel();
            jLabel.setText(eingaben[i]);
            jLabel_list[i] = jLabel;


            JTextField jTextField = new JTextField();
            jTextField.setPreferredSize(new Dimension(150, 30));
            jTF_list[i] = jTextField;

            JButton jButton = createJButton(i, jTextField);
            button_list[i] = jButton;

            jPanel.add(jLabel);
            jPanel.add(jTextField);
            jPanel.add(jButton);
            center_panel.add(jPanel, BorderLayout.CENTER);
        }
        return center_panel;
    }

    @NotNull
    private static JFrame initFrame() {
        JFrame jFrame = new JFrame();
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(700, 400);
        jFrame.setLayout(new BorderLayout());
        jFrame.setVisible(true);
        return jFrame;
    }


    public static JButton createJButton(int type, JTextField jTF) {
        JButton jButton = new JButton();
        jButton.setText("Check");
        switch (type) {
            case 0:
            case 1:
                jButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (StyleCheck.checkName(jTF.getText())) {
                            jTF.setBackground(Color.green);
                        } else {
                            jTF.setBackground(Color.red);
                        }
                    }
                });
                break;
            case 2:
                jButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (StyleCheck.checkUsername(jTF.getText())) {
                            jTF.setBackground(Color.green);
                        } else {
                            jTF.setBackground(Color.red);
                        }
                    }
                });
                break;
            case 3:
                jButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (jTF.getText().equals("Männlich") || jTF.getText().equals("Weiblich")) {
                            jTF.setBackground(Color.green);
                        } else {
                            jTF.setBackground(Color.red);
                        }
                    }
                });
                break;
            case 4:
                jButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (StyleCheck.checkAddress(jTF.getText())) {
                            jTF.setBackground(Color.green);
                        } else {
                            jTF.setBackground(Color.red);
                        }
                    }
                });
                break;
            case 5:
                jButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (StyleCheck.checkCity(jTF.getText())) {
                            jTF.setBackground(Color.green);
                        } else {
                            jTF.setBackground(Color.red);
                        }
                    }
                });
                break;
            case 6:
                jButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (StyleCheck.checkPLZ(jTF.getText())) {
                            jTF.setBackground(Color.green);
                        } else {
                            jTF.setBackground(Color.red);
                        }
                    }
                });
                break;
            case 7:
                jButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (StyleCheck.checkBirthday(jTF.getText())) {
                            jTF.setBackground(Color.green);
                        } else {
                            jTF.setBackground(Color.red);
                        }
                    }
                });
                break;
        }
        return jButton;
    }
}
