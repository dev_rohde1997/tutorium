package vorkurs.gui;

import vorkurs.Caesar;
import vorkurs.Vigenere;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KryptoGUI {
    public static void main(String[] args) {

        JFrame jFrame = new JFrame();
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setPreferredSize(new Dimension(500, 500));
        jFrame.setSize(600, 600);
        jFrame.setLocation(500, 200);
        jFrame.setLayout(new BorderLayout());

        //INPUT
        JTextArea input = new JTextArea();
        input.setFont(new Font("mFont", Font.PLAIN, 25));
        input.setBorder(BorderFactory.createLineBorder(Color.black, 2, true));

        //HEADLINE
        JLabel headline = new JLabel();
        headline.setText("Mein Verschlüsselungsprogramm");
        headline.setFont(new Font("MyFont", Font.PLAIN, 15));
        headline.setHorizontalAlignment(SwingConstants.CENTER);

        //BOTTOM
        Container bottom_main_container = new Container();
        bottom_main_container.setLayout(new GridLayout(2, 1));

        //Choice Container
        Container choice_container = new Container();
        choice_container.setLayout(new FlowLayout());

        JLabel key_label = new JLabel("Key:");
        key_label.setVisible(false);
        JTextField key_input = new JTextField();
        key_input.setPreferredSize(new Dimension(100, 20));
        key_input.setVisible(false);

        JRadioButton caesar_radio_button = new JRadioButton();
        caesar_radio_button.setText("Caesar");
        caesar_radio_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                key_label.setVisible(false);
                key_input.setText("");
                key_input.setVisible(false);
            }
        });

        JRadioButton vigenere_radio_button = new JRadioButton();
        vigenere_radio_button.setText("Vigenère");
        vigenere_radio_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                key_label.setVisible(true);
                key_input.setVisible(true);
            }
        });

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(caesar_radio_button);
        buttonGroup.add(vigenere_radio_button);

        choice_container.add(caesar_radio_button);
        choice_container.add(vigenere_radio_button);
        choice_container.add(key_label);
        choice_container.add(key_input);


        //Button Container
        Container button_container = new Container();
        button_container.setLayout(new FlowLayout());


        //ENCRYPT
        JButton btn_encrypt = new JButton();
        btn_encrypt.setText("Verschlüsseln");
        btn_encrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (caesar_radio_button.isSelected()) {
                    String new_word = Caesar.encrypt(input.getText());
                    input.setText(new_word);
                }

                if (vigenere_radio_button.isSelected()) {
                    String new_word = Vigenere.encrypt(input.getText(), key_input.getText());
                    input.setText(new_word);
                }

            }
        });

        //DECRYPT
        JButton btn_decrypt = new JButton();
        btn_decrypt.setText("Entschlüsseln");
        btn_decrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (caesar_radio_button.isSelected()) {
                    String new_word = Caesar.decrypt(input.getText());
                    input.setText(new_word);
                }

                if (vigenere_radio_button.isSelected()) {
                    String new_word = Vigenere.decrypt(input.getText(), key_input.getText());
                    input.setText(new_word);
                }
            }
        });

        button_container.add(btn_encrypt);
        button_container.add(btn_decrypt);


        bottom_main_container.add(choice_container);
        bottom_main_container.add(button_container);

        jFrame.add(headline, BorderLayout.NORTH);
        jFrame.add(bottom_main_container, BorderLayout.SOUTH);
        jFrame.add(input, BorderLayout.CENTER);

        jFrame.validate();
        jFrame.repaint();

    }
}
