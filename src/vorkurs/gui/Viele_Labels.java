package vorkurs.gui;

import javax.swing.*;
import java.awt.*;

public class Viele_Labels {
    public static void main(String[] args) {

        //Variablen
        JLabel result_label = new JLabel();

        int breite = 10;
        int hoehe = 10;


        JFrame jFrame = new JFrame();
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(300, 300);
        jFrame.setLayout(new GridLayout(breite, hoehe));

        for (int i = 0; i < breite * hoehe; i++) {
            JLabel jLabel = new JLabel();
            jLabel.setText(""+i);
            jFrame.add(jLabel);
        }

        jFrame.setVisible(true);
    }
}
