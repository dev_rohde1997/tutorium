package vorkurs;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int[] liste = new int[10];

        //Zahlen eingeben und speichern
        for (int i = 0; i < 10; i++) {
            System.out.print("Zahl eingeben: ");
            liste[i] = scanner.nextInt();
        }

        //int[] liste = {11, 22, 33, 44, 55, 121, 123, 1234};


        //Ausgeben
        for (int zahl: liste) {
            System.out.print(zahl + ", ");
        }
        System.out.println();

        //Palindrom - String
        palindrom_string(liste);


        //Palindrom - StringBuilder
        palindrom_stringbuilder(liste);
    }

    private static void palindrom_stringbuilder(int[] liste) {
        for (int zahl: liste) {
            StringBuilder string = new StringBuilder(zahl);
            String revesed = "" + string.reverse();
            int zahl_neu = Integer.parseInt(revesed);


            if (zahl_neu == zahl){
                System.out.printf("%d ist eine Palindromzahl", zahl);
            } else{
                System.out.printf("%d ist keine Palindromzahl", zahl);
            }

        }
    }

    private static void palindrom_string(int[] liste) {
        //Palindrom - String
        for (int zahl : liste) {
            String zahl_als_string = "" + zahl;
            String string_neu = "";

            System.out.printf("Alter String: %s%n", zahl_als_string);

            for (int i = zahl_als_string.length() - 1; i >= 0; i--) {
                string_neu += zahl_als_string.charAt(i);
            }
            System.out.printf("Neuer String: %s%n", string_neu);

            int neue_zahl = Integer.parseInt(string_neu);

            if (neue_zahl == zahl) {
                System.out.printf("%d ist eine Palindromzahl", zahl);
            } else {
                System.out.printf("%d ist keine Palindromzahl", zahl);
            }
        }
    }

    private boolean is_palindrom(int zahl) {
        String zahl_als_string = "" + zahl;
        String string_neu = "";

        //System.out.printf("Alter String: %s%n",zahl_als_string);

        for (int i = zahl_als_string.length() - 1; i >= 0; i--) {
            string_neu += zahl_als_string.charAt(i);
        }
        //System.out.printf("Neuer String: %s%n", string_neu);

        int neue_zahl = Integer.parseInt(string_neu);

        return zahl == neue_zahl;
    }

}
