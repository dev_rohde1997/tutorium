package vorkurs.vier_gewinnt;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Field field = new Field();
        field.print();
        Player player1 = new Player('O', "Player1");
        Player player2 = new Player('X', "Player2");

        Scanner sc = new Scanner(System.in);

        while (!field.checkWin()) {

            //Spielfeld printen

            field.print();
            //Spielzug Player 1
            turn(field, player1);
            if (field.checkWin())
                break;

            //Spielzug Player 2
            turn(field, player2);

        }

        System.out.println("Spieler " + field.getWinner() + " hat gewonnen.");
    }

    static void turn(Field field, Player player) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Spieler " + player.getName() + " ist an der Reihe");
        System.out.print("Bitte Reihe eingeben: ");
        int column = sc.nextInt();
        if (column == -1 )
            System.exit(0);

        if (!field.setField(column, player.getSymbol()))
            turn(field, player);

    }
}
