package vorkurs.vier_gewinnt;

public class Field {
    private char[][] field;
    private char winner;

    Field() {
        field = new char[6][7];
        init();
    }

    private void init() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = '_';
            }
        }
    }

    public void print() {
        System.out.println(" -----------------------------");
        System.out.println(" | 0   1   2   3   4   5   6 |");
        for (int i = 0; i < field.length; i++) {
            //System.out.print("  ");
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(" | ");
                System.out.print(field[i][j]);
                if (j == field[i].length - 1) {
                    System.out.println(" |");
                }

            }

        }
        System.out.println(" -----------------------------");
        System.out.println();
    }

    public boolean setField(int column, char new_char) {
        if (field[0][column] != '_') return false;

        for (int i = field.length - 1; i >= 0; i--) {
            if (field[i][column] == '_') {
                field[i][column] = new_char;
                break;
            }
        }
        print();
        return true;
    }

    public char getField(int row, int column) {
        return field[column][row];
    }

    public boolean checkWin() {
        if (checkWinHorizontal('O') |
            checkWinHorizontal('X') |
            checkWinVertical('O') |
            checkWinVertical('X') |
            checkDiagonal('X') |
            checkDiagonal('O'))
            return true;
        return false;
    }

    private boolean checkWinVertical(char symbol) {
        for (int row = 0; row < field.length; row++) {
            for (int column = 0; column < field[row].length - 4; column++) {
                if (getField(row, column) == symbol &&
                    getField(row, column+1) == symbol &&
                    getField(row, column+2) == symbol &&
                    getField(row, column+3) == symbol
                ) {
                    winner = symbol;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkWinHorizontal(char symbol) {
        for (int column = 0; column < field.length; column++) {
            for (int row = 0; row < field[column].length - 4; row++) {
                if (getField(row, column) == symbol &&
                    getField(row + 1, column) == symbol &&
                    getField(row + 2, column) == symbol &&
                    getField(row + 3, column) == symbol
                ) {
                    winner = symbol;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkDiagonal(char symbol) {
        for (int column = 0; column < field.length - 3; column++) {
            for (int row = field[column].length - 1; row > 3; row--) {

                if (getField(row, column) == symbol &&
                        getField(row - 1, column + 1) == symbol &&
                        getField(row - 2, column + 2) == symbol &&
                        getField(row - 3, column + 3) == symbol
                ) {
                    winner = symbol;
                    return true;
                }
            }
        }

        for (int column = field.length - 1; column > 3; column--) {
            for (int row = 0; row < field[column].length - 3; row++) {

                if (getField(row, column) == symbol &&
                        getField(row + 1, column - 1) == symbol &&
                        getField(row + 2, column - 2) == symbol &&
                        getField(row + 3, column - 3) == symbol
                ) {
                    winner = symbol;
                    return true;
                }
            }
        }


        return false;
    }

    public char getWinner() {
        return winner;
    }

    public void setWinner(char winner) {
        this.winner = winner;
    }
}
