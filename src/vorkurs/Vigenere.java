package vorkurs;

public class Vigenere {
    public static void main(String[] args) {

        String text = "Das ist ein super geheimer Text und den darf niemand lesen";
        String key = "sssdv";


        System.out.println("Original: " + text);

        String encrypted = encrypt(text, key);
        System.out.println("Verschl: " + encrypted);

        System.out.println("Entschl: " + decrypt(encrypted, key));

    }

    public static String encrypt(String word, String key) {
        String out = "";
        int pointer = 0;

        //Calculate key
        int[] key_values = calculateKey(key);

        if (key_values == null) return "Invalid key";

        for (char c : word.toCharArray()) {
            int char_als_int = (int) c;
            int moves = key_values[pointer % key_values.length];

            while (moves > 0) {

                if (char_als_int == 126) {
                    char_als_int = 32;
                    moves--;
                    continue;
                }

                char_als_int++;
                moves--;
            }
            pointer++;
            out += (char) char_als_int;
        }

        return out;
    }

    private static int[] calculateKey(String key) {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        key = key.toUpperCase();
        int[] key_values = new int[key.length()];
        a: for (int i = 0; i < key.length(); i++) {
            for (int pos = 0; pos < alphabet.length(); pos++) {
                if (alphabet.charAt(pos) == key.charAt(i)) {
                    key_values[i] = pos;
                    continue a;
                }
            }
            return null;
        }
        return key_values;
    }

    public static String decrypt(String word, String key) {
        StringBuilder out = new StringBuilder();
        int pointer = 0;

        int[] key_values = calculateKey(key);
        if (key_values == null) return "Invalid key";


        for (char c : word.toCharArray()) {
            int char_as_int = (int) c;
            int moves = key_values[pointer % key_values.length];
            while (moves > 0) {

                if (char_as_int == 32) {
                    char_as_int = 126;
                    moves--;
                    continue;
                }

                char_as_int--;
                moves--;
            }
            pointer++;
            out.append((char) char_as_int);
        }
        return out.toString();
    }
}
