package vorkurs;

import java.util.Scanner;

public class Primzahl {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        //Eingabe
        int eingabe = scanner.nextInt();
        boolean is_prime = true;


        if (eingabe < 2) {
            is_prime = false;
        }

        for(int i = 2; i < eingabe; i++) {
            if (eingabe % i == 0) {
                is_prime = false;
                break;
            }
        }

        System.out.print(eingabe);
        if (is_prime) {
            System.out.println(" ist eine Primzahl");
        } else {
            System.out.println(" ist keine Primzahl");
        }

    }
}
