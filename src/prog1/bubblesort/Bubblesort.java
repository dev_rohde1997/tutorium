package prog1.bubblesort;

/**
 * @author Moritz Rohde
 * Das Programm enthält eine Algorithmus der ein Array aufsteigend sortiert.
 * Die Sortierung erfolt nach Bubblesort
 */

public class Bubblesort {

    int[] list;

    public Bubblesort(int[] list) {
        this.list = list;
    }

    public static void main(String[] args) {
        int[] liste = {56,44,34,29,19,27,99,65,48};
        //Create sorting class
        Bubblesort bubblesort = new Bubblesort(liste);

        //Print raw numbers
        bubblesort.print(", ");

        //Sorting
        bubblesort.bubbleSort();

        //Output
        bubblesort.print(", ");

    }

    public int[] bubbleSort() {
        if (sorted()) {
            return list;
        } else {
            for (int i = 0; i < list.length - 1; i++) {
                if (list[i] > list[i + 1]) {
                    int j = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = j;
                }
            }
            bubbleSort();
        }
        return list;
    }

    private boolean sorted() {
        for (int i = 0; i < list.length - 1; i++) {
            if (list[i] > list[i + 1]) {
                return false;
            }
        }
        return true;
    }

    public void print(String sep) {
        System.out.println("Numbers:");
        for (int k : list) {
            if (k == list[0])
                System.out.print(k);
            else
                System.out.print(sep + k);
        }
        System.out.println();
    }


}
