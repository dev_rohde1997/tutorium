package prog1.exceptions;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionMain {
    public static void main(String[] args) {
        try {
            readNumber();
            writeFile("Test.txt", "Das ist mein Beispieltext");
            readFile("Test.txt");

        } catch (IOException io) {
                io.printStackTrace();
        }  finally {
            System.out.println("ENDE");
        }
    }

    static int readNumber() throws InputMismatchException {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    static String readFile(String path) throws IOException {
        Scanner sc = new Scanner(Paths.get(path));
        String line = "";
        while (sc.hasNext()) {
            line += sc.nextInt();
        }
        return line;

    }

    static void writeFile(String path, String text) throws IOException{
        BufferedWriter bf;
        bf = Files.newBufferedWriter(Paths.get(path));
        bf.write(text);
        bf.close();
    }


}
