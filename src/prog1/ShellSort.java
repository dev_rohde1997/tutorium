package prog1;

public class ShellSort {
    public static void main(String[] args) {
        int[] values = {4,3,7,6,9,7,4,2,5,4};
        for (int i : values) System.out.print(i + ", ");
        int[][] sortedInt = shellsort(values);

        //print Array
        System.out.println("\nSorted Print");
        for (int[] i : sortedInt) {
            System.out.println();
            for (int j : i) {
                System.out.print(j + ", ");
            }
        }
        System.out.println();



    }

    public static int[][] shellsort(int[] list) {

        int[][] two_dimensional_array = new int[0][0];

        for (int i = 0; i < list.length; i++) {
            int add_values[] = {list[i], i};
            two_dimensional_array = append(two_dimensional_array, add_values);
        }

        //print Array
        System.out.println("\nAfter Append Print: ");
        for (int[] i : two_dimensional_array) {
            System.out.println();
            for (int j : i) {
                System.out.print(j + ", ");
            }
        }

        int n = two_dimensional_array.length;
        int gap = Math.floorDiv(n, 2);
        while (gap > 0) {
            for (int i = gap; i < n; i++) {
                int[] temp = two_dimensional_array[i];
                int j = i;
                while (j >= gap && compare(two_dimensional_array[j-gap][0], temp[0])) {
                    two_dimensional_array[j] = two_dimensional_array[j-gap];
                    j -= gap;
                }
                two_dimensional_array[j] = temp;
            }
            gap = Math.floorDiv(gap, 2);
        }
        return two_dimensional_array;
    }

    public static boolean compare(int value1, int value2) {
        return  value1 > value2;
    }

    public static int[][] append(int[][] list, int[] values) {
        int[][] temp = new int[list.length + 1][];
        for (int i = 0; i < list.length; i++) {
            temp[i] = list[i];
            for (int j = 0; j < list[i].length; j++) {
                temp[i][j] = list[i][j];
            }
        }
        temp[list.length] = values;
        return temp;
    }


}
