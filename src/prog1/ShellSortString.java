package prog1;

public class ShellSortString {
    public static void main(String[] args) {
        String[] values = {"A", "C", "B", "Z", "D", "Ab", "Aa"};
        for (String i : values) System.out.print(i + ", ");
        String[][] sortedInt = shellsort(values);

        //print Array
        System.out.println("\nSorted Print");
        for (String[] i : sortedInt) {
            System.out.println();
            for (String j : i) {
                System.out.print(j + ", ");
            }
        }
        System.out.println();

    }

    public static String[][] shellsort(String[] list) {

        String[][] two_dimensional_array = new String[0][0];

        for (int i = 0; i < list.length; i++) {
            String add_values[] = {list[i], String.valueOf(i)};
            two_dimensional_array = append(two_dimensional_array, add_values);
        }

        //print Array
        System.out.println("\nAfter Append Print: ");
        for (String[] i : two_dimensional_array) {
            System.out.println();
            for (String j : i) {
                System.out.print(j + ", ");
            }
        }

        int n = two_dimensional_array.length;
        int gap = Math.floorDiv(n, 2);
        while (gap > 0) {
            for (int i = gap; i < n; i++) {
                String[] temp = two_dimensional_array[i];
                int j = i;
                while (j >= gap && compare(two_dimensional_array[j-gap][0], temp[0])) {
                    two_dimensional_array[j] = two_dimensional_array[j-gap];
                    j -= gap;
                }
                two_dimensional_array[j] = temp;
            }
            gap = Math.floorDiv(gap, 2);
        }
        return two_dimensional_array;
    }

    public static boolean compare(String value1, String value2) {
        int length;
        if (value1.length() < value2.length()) {
            length = value1.length();
        } else {
            length = value2.length();
        }
        int i = 0;
        while (i < length && value1.charAt(i) == value2.charAt(i)) i++;
        if (i == length) {
            return value1.length() != length;
        }
        return value1.charAt(i) > value2.charAt(i);
    }

    public static String[][] append(String[][] list, String[] values) {
        String[][] temp = new String[list.length + 1][];
        for (int i = 0; i < list.length; i++) {
            temp[i] = list[i];
            for (int j = 0; j < list[i].length; j++) {
                temp[i][j] = list[i][j];
            }
        }
        temp[list.length] = values;
        return temp;
    }


}
