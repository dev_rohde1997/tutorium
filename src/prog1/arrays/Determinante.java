package prog1.arrays;

public class Determinante {
    public static void main(String[] args) {
        int[][] matrix = new int[2][2];

        matrix[0][0] = 1;
        matrix[0][1] = 2;
        matrix[1][0] = 3;
        matrix[1][1] = 4;



        print(matrix);

        System.out.printf("%n\tDie Determinante ist: %.2f%n%n", det(matrix));
    }

    public static void print(int[][] matrix) {
        System.out.printf("Matrix:%n");
        //Ausgabe der Matrix
        printMatrix(matrix);

    }

    public static void print_Horizontal(int[][] matrix, char c) {
        System.out.print("\n\t");
        for (int k = 0; k < 8 * matrix.length + matrix.length; k++)
            System.out.print(c);
        System.out.println();
    }

    public static void printMatrix(int[][] matrix) {
        System.out.print("\t\t");
        for (int i = 0; i < matrix.length; i++) {
            System.out.printf("%d\t  \t", i);
        }
        print_Horizontal(matrix, '=');

        for (int i = 0; i < matrix.length; i++) {
            System.out.printf("%d\t| \t", i);
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf("%d\t||\t", matrix[i][j]);
            }
            print_Horizontal(matrix, '-');
        }
    }

    public static double det(int[][] matrix) {
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    }


}