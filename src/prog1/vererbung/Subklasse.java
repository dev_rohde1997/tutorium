package prog1.vererbung;

public class Subklasse extends UltraKlasse {
    String test;

    Subklasse(int id, String name, String test) {
        super(id, name);
        this.test = test;
    }

    @Override
    public String werBinIch() {
        String output = "";
        output += super.werBinIch();
        output += "Ich bin eine Subklasse";
        return output;
    }
}
