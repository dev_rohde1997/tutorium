package prog1.vererbung;

public class UltraKlasse {
    private int id;
    private String name = null;

    public int getId() {
        return id;
    }



    UltraKlasse(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String werBinIch() {
        return String.format("Ich bin eine ULTRA Klasse");
    }

    public String getName() {
        return name;
    }
}
