package prog1.objektorientierung;

public class Werkzeuge {
    private String name;
    private int id;

    public Werkzeuge(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String toString() {
        return String.format("Ich bin %s. ID: %d", getClass().getName(), id);
    }

    public int getId() {
        return id;
    }

}
