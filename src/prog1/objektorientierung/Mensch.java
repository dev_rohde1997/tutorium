package prog1.objektorientierung;

public class Mensch {
    private String name;
    Geburtstag geburtstag = new Geburtstag(17, 9, 1997);

    Mensch() {
        System.out.println("Ein neuer Mensch wurde geboren");
    }

    Mensch(String name, Geburtstag geburtstag) {
        this.name = name;
        this.geburtstag = geburtstag;
    }

    Mensch(String name) {
        this.name = name;
    }

    public String toString() {
        return "Ich bin " + name + " und am " + geburtstag + " geboren";
    }
}

class Geburtstag {
    int tag;
    int monat;
    int jahr;

    public Geburtstag(int tag, int monat, int jahr) {
        this.tag = tag;
        this.monat = monat;
        this.jahr = jahr;
    }

    @Override
    public String toString() {
        return String.format("%d/%d/%d", tag, monat, jahr);
    }
}
