package prog1.objektorientierung.abstract_ex;

public abstract class Base {


    public abstract void getBaseInformation();

    public abstract int getId();

    public String toString() {
        return "Base";
    }
}
