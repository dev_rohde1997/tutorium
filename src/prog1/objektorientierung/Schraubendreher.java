package prog1.objektorientierung;

public class Schraubendreher extends Werkzeuge {
    private boolean ist_kreuz_schlitz;

    public Schraubendreher(String name, int id, boolean ist_kreuz_schlitz) {
        super(name, id);
        this.ist_kreuz_schlitz = ist_kreuz_schlitz;
    }
}
