package prog1.objektorientierung;

public class Hammer extends Werkzeuge {
    private int gewicht;

    public Hammer(String name, int id, int gewicht) {
        super(name, id);
        this.gewicht = gewicht;
    }

    @Override
    public String toString() {
        return super.toString() + "\nHAMMER!!!";
    }

    void ausgabe() {
        System.out.println(getId());
    }
}
