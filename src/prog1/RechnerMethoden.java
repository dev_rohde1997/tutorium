package prog1;

/**
 * @author Moritz Rohde
 */
public class RechnerMethoden {
    public static void main(String[] args) {

        if (args.length < 2) {
            System.out.println("Zu wenig Zahlen übergeben");
            return;
        }

        //Beispiel mit int: int zahl1 = Integer.parseInt(args[0]);
        double zahl1 = Double.parseDouble(args[0]);
        double zahl2 = Double.parseDouble(args[1]);


        System.out.printf("Ich rechne: %.2f + %.2f = %.2f%n", zahl1, zahl2, addieren(zahl1, zahl2));
        System.out.printf("Ich rechne: %.2f - %.2f = %.2f%n", zahl1, zahl2, subtrahieren(zahl1, zahl2));
        System.out.printf("Ich rechne: %.2f * %.2f = %.2f%n", zahl1, zahl2, multipilzieren(zahl1, zahl2));
        System.out.printf("Ich rechne: %.2f / %.2f = %.2f%n", zahl1, zahl2, dividieren(zahl1, zahl2));
    }


    static double addieren (double zahl1, double zahl2) {
        double ergebnis = zahl1 + zahl2;
        return ergebnis;
    }
    static double subtrahieren (double zahl1, double zahl2) {
        return zahl1 - zahl2;
    }
    static double multipilzieren (double zahl1, double zahl2) {
        return zahl1 * zahl2;
    }
    static double dividieren (double zahl1, double zahl2) {
        if (zahl2 == 0) {
            System.out.println("Teilen durch 0 ist nicht erlaubt!");
            return -1;
        }
        return zahl1 / zahl2;
    }


}
