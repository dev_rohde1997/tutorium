package prog1.interfaces;

public class MainInterface extends MainInterface2 {
    public static void main(String[] args) {
        }
}

class Feld {
    String[][] feld = new String[10][10];
}

class Test implements MeinInterface {

    @Override
    public String aussage() {
        return "Das ist eine Beispiel ausgabe: " + getId();
    }

    public void test() {
        System.out.println(MeinInterface.KONSTANTE);
        System.out.println(standard_port);
    }
}



interface MeinInterface {

    final int standard_port = 8000;
    public static final String KONSTANTE = "Mein Interrface";


    public String aussage();

    public default double getId() {
        return Math.random() * 100;
    }
}