package prog1.interfaces;

public class Main {
    public static void main(String[] args) {
        Kindklasse kind = new Kindklasse("Kind", 0);
        kind.getInfo();
        System.out.println(kind.getId());
    }
}


abstract class Mutterklasse {
    private String name;
    private int id;

    public abstract void getInfo();

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    Mutterklasse(String name, int id) {
        this.name = name;
        this.id = id;
    }
}

class Kindklasse extends Mutterklasse{


    Kindklasse(String name, int id) {
        super(name, id);
    }

    @Override
    public void getInfo() {
        System.out.println("Info");
    }
}