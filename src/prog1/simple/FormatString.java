package prog1.simple;

public class FormatString {
    public static void main(String... args) {
        if (args.length == 0) throw new IllegalArgumentException();
        try {
            String formattedString = formatString(args[0]);
            if (formattedString.length() >= args[0].length())
                throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            System.err.println("Möp");
        }

    }

    public static String formatString(String s) {
        String output = "";
        int count = 1;
        for (int i = 0; i < s.length(); i++) {
            count = 1;
            output += s.charAt(i);

            while (i + 1 < s.length() & s.charAt(i + 1) == s.charAt(i)) {
                count++;
                i++;
            }
            output += count;
        }
        return output;
    }
}
