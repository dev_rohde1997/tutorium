package prog1.simple;

import java.util.Scanner;

public class CheckNumber {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        System.out.println("Geben Sie bitte eine Zahl ein: ");
        int input = scanner.nextInt();

        if(input % 2 == 0) {
            System.out.printf("%d ist gerade%n", input);
        } else {
            System.out.printf("%d ist ungerade%n", input);
        }

        for (int teiler = 2; teiler < input; teiler++) {
            if (input % teiler == 0) {
                flag = false;
                break;
            }
        }

        //Ausgabe
        if (!flag) {
            System.out.printf("%d ist keine Primzahl%n", input);
        } else {
            System.out.printf("%d ist eine Primzahl%n", input);
        }
    }
}


