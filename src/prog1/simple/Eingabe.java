package prog1.simple;

public class Eingabe {
    public static void main(String[] args) {

        //Definieren
        int[] liste = new int[5];


        //Ausführen

        //reinschreiben
        for (int i = 0; i < liste.length; i++) {
            liste[i] = i * 5;
        }

        //Auslesen
        for (int i = 0; i < liste.length; i++) {
            System.out.println("Liste an der Stelle " + i + " -> " + liste[i]);

        }

    }
}
