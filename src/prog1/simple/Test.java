package prog1.simple;

public class Test {

    public static void main(String[] args) {
        Test2 test2 = new Test2();
        if (test2 instanceof Methoden) {
            System.out.println("Jaaa");
            test2.move(5);
        } else {
            System.out.println("Nein");

        }
    }
}

class Test2 implements Methoden {
    @Override
    public void getX() {

    }

    public boolean move(int position) {
        return true;
    }
}


interface Methoden {
    public void getX();
    public boolean move(int position);
}