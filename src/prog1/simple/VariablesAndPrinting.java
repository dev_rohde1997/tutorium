package prog1.simple;

public class VariablesAndPrinting {

    public static void main(String[] args) {
        //Variables

        String first_letter_of_fore_name = "M";
        String surname = "Rohde";
        int age = 22;
        double body_height = 1.79;
        boolean full_age = false;
        String full_age_str = "volljährig";


        if (age < 18) {
            full_age = true;
            full_age_str = "nicht " + full_age_str;
            //volljährig -> nicht volljährig
        } else if (age > 80) {
            full_age_str = "volljährig, aber super alt";
        } else {
            full_age_str = "volljährig";
        }

        //println
        System.out.println("Println:");
        System.out.println("Mein Name ist " + first_letter_of_fore_name + "." + surname + ".");
        System.out.println("Ich bin " + age + " und damit " + full_age_str);
        System.out.println("Ich bin " + body_height + " gross." + "\n\n");

        //printf
        System.out.printf("Printf:%n");
        System.out.printf("Mein Name ist %s.%s.%n", first_letter_of_fore_name, surname);
        System.out.printf("Ich bin %d Jahre alt und damit %s%n", age, full_age_str);
        System.out.printf("Ich bin %.2fcm gross%n", body_height);


    }
}
