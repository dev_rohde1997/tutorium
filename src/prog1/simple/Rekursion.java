package prog1.simple;

public class Rekursion {

    public static void main(String args[]) {
        System.out.println(f(16, 3));
    }

    static int f(int x, int y) {
        System.out.printf("X:%d Y:%d%n", x, y);
        if (y <= 1)
            return y + 2;
        else if (x == 1)
            return x + 1;
        else
            return 2 + f(f(x - 3, y), y / 2);
    }
}
