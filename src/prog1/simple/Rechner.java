package prog1.simple;

import java.util.Scanner;

public class Rechner {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Geben Sie die erste Zahl ein:");
        int zahlen_eingabe_1 = sc.nextInt();
        System.out.println();
        System.out.print("Geben Sie die zweite Zahl ein:");
        int zahlen_eingabe_2 = sc.nextInt();
        System.out.println();

        addieren(zahlen_eingabe_1, zahlen_eingabe_2);

        subtrahieren(zahlen_eingabe_1, zahlen_eingabe_2);

        System.out.printf("%d * %d = %d%n", zahlen_eingabe_1, zahlen_eingabe_2,
                multiplizieren(zahlen_eingabe_1, zahlen_eingabe_2));

        System.out.printf("%d / %d = %.3f%n", zahlen_eingabe_1, zahlen_eingabe_2,
                dividieren(zahlen_eingabe_1, zahlen_eingabe_2));

    }

    public static void addieren(int zahl1, int zahl2) {
        System.out.printf("%d + %d = %d%n", zahl1, zahl2, zahl1 + zahl2);
    }

    public static void subtrahieren(int zahl1, int zahl2) {
        int ergebnis = zahl1 - zahl2;
        System.out.printf("%d - %d = %d%n", zahl1, zahl2, ergebnis);
    }

    public static int multiplizieren(int zahl1, int zahl2) {
        return zahl1 * zahl2;
    }

    public static double dividieren(int zahl1, int zahl2) {
        if (zahl2 == 0) {
            System.err.println("Durch 0 teilen ist nicht zulässig!");
            return 0;
        }
        //Speichern in double, damit das Ergebnis auch Nachkommstellen hat
        double d_zahl1 = zahl1;
        double d_zahl2 = zahl2;
        return d_zahl1 / d_zahl2;
    }
}
