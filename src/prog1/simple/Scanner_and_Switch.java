package prog1.simple;

import java.util.Scanner;

public class Scanner_and_Switch {

    public static void main(String[] args) {
        //Variables
        Scanner sc = new Scanner(System.in);

        //First name
        System.out.print("Enter your first name: ");
        char first_letter_of_fore_name = sc.next().charAt(0);
        System.out.println();
        //Surname
        System.out.print("Enter your surname: ");
        String surname = sc.next();
        System.out.println();
        //Age
        System.out.print("Enter your age: ");
        int age = sc.nextInt(); //Byte contains -256 to 255, good datatype for an age
        System.out.println();
        //Body Height
        System.out.print("Enter your body height in x,yzcm: ");
        double body_height = sc.nextDouble();
        //Full age
        boolean full_age = false;

        if (age >= 18) {
            full_age = true;
        }

        //Output
        System.out.println("\n\n\n\n\nWhat do you want to know?");
        System.out.print("Allowed input: name, age, height: ");
        String output_chose = sc.next();
        System.out.println("\nResult:");

        //switch
        switch (output_chose) {
            case "name":
                System.out.printf("My name is %c.%s.%n", first_letter_of_fore_name, surname);
                break;
            case "age":
                System.out.printf("I am %d years old, so I am %sfull age%n", age, full_age ? "" : "nicht ");
                break;
            case "height":
                System.out.printf("I am %.2fcm height%n", body_height);
                break;
            default:
                System.out.printf("Error: Wrong input!");
                break;

        }
    }
}
