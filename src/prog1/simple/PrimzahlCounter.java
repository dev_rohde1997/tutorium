package prog1.simple;

public class PrimzahlCounter {


    public static void main(String[] args) {
        int range_of_numbers = 50;
        boolean full_output = true;

        m:
        for (int number = 0; number <= range_of_numbers; number++) {
            if (number < 2) {
                if (full_output) {
                    System.out.printf("Number \t%d\t is not a prime number!%n", number);
                }
                continue m;
            }
            for (int divisor = 2; divisor < number; divisor++) {
                if (number % divisor == 0) {
                    if (full_output) {
                        System.out.printf("Number \t%d\t is not a prime number!%n", number);
                    }
                    continue m;
                }
            }
            System.out.printf("Number \t%d\t is a prime number!%n", number);

        }
    }
}