package prog2.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MyReader {

    String path = "/Users/moritz/IdeaProjects/Tutorium/src/prog2/MyReader.java";
    BufferedReader bf;

    MyReader() {
        try {
            this.bf = Files.newBufferedReader(Paths.get(path));
        } catch (IOException ie) {
            System.err.println("Fehler");
        }

    }


    public String auslesen() throws IOException {
        String line = null;
        String data = "";
        while ((line = bf.readLine()) != null) {
            System.out.println(line);
            data += line;
        }
        return data;
    }
}
