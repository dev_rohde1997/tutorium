package prog2.io;

import java.io.*;

public class MyFileInputStream extends InputStream {

    private FileInputStream stream;
    private String read_input = "";

    MyFileInputStream(String path) {
        super();
        init(path);
    }

    private void init(String path) {
        try {
            stream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int read() throws IOException {
        return stream.read();
    }


    public String readFile() {
        try {
            while(stream.available() > 0) {
                read_input += (char) read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return read_input;
    }




}
