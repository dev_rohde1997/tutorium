package prog2.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Output_test {
    public static void main(String[] args) {

        try {
            FileInputStream fileInputStream = new FileInputStream("Test.txt");

            while(fileInputStream.available() > 0) {
                System.out.println( fileInputStream.read());

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
