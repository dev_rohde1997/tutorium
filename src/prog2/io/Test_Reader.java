package prog2.io;

//import org.junit.Clone;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

//import static org.junit.Assert.assertEquals;

public class Test_Reader {

    MyFileInputStream reader;

    public Test_Reader() {
        super();
    }

    private void init() {
        reader = new MyFileInputStream("OK.txt");
    }

    //@Clone
    public void test_read() {
        String test_data = "Das ist ein Clone";
        try {
            //Write
            FileOutputStream writer = new FileOutputStream("OK.txt");
            writer.write(test_data.getBytes());
            writer.flush();
            writer.close();

            //Read
            init();
            //assertEquals(test_data, reader.readFile());
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
