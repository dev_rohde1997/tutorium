package prog2.io;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String... args) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("Clone.txt");
            String data = "Clone Hallo Welt";
            fileOutputStream.write(data.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
