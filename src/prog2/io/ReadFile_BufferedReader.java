package prog2.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadFile_BufferedReader {

    public static void main(String[] args) {

        //Your file path
        String absolute_path = "/Users/moritz/IdeaProjects/Tutorium/src/prog2/ReadFile_BufferedReader.java";
        String relative_path = "src/prog2/ReadFile_BufferedReader.java";
        String line = null;

        try {
            //String currentDir = System.getProperty("user.dir");
            //System.out.println("Current dir using System:" +currentDir);

            BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(relative_path));

            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
            if (line == null) throw new IllegalArgumentException("Leere Datei");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
