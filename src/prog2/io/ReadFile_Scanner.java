package prog2.io;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class ReadFile_Scanner {

    public static void main(String[] args) {

        //Your file path
        String absolute_path = "/Users/moritz/IdeaProjects/Tutorium/src/prog2/readme.txt";
        String line = null;

        Scanner scanner = null;
        try {
            scanner = new Scanner(Paths.get(absolute_path));
        } catch (IOException e) {
            System.err.println("Fehler");
        }

        while (scanner.hasNext()) {
            System.out.println(scanner.nextLine());
        }
    }
}


