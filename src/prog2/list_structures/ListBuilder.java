package prog2.list_structures;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class ListBuilder {
    public static void main(String[] args) {

        try {
            //Namen auslesen
            Scanner name_scanner = new Scanner(Paths.get("Names.txt"));
            ArrayList<String> nameList = new ArrayList<>();
            while (name_scanner.hasNext()) {
                nameList.add(name_scanner.next());
            }


            //Zahlen einlesen
            ArrayList<Integer> idList = new ArrayList<>();
            Scanner number_scanner = new Scanner(Paths.get("Ids.txt"));
            while (number_scanner.hasNextInt()) {
                idList.add(number_scanner.nextInt());
            }

            HashMap<String, Integer> person_data = new HashMap<>();
            for (int i = 0; true; i++) {
                if (i >= nameList.size() || i >= idList.size())
                    break;
                person_data.put(nameList.get(i), idList.get(i));
            }

            //Sortierte Ausgabe der Namen
            String[] names = new String[nameList.size()];
            nameList.toArray(names);

            System.out.println("Namen (unsortiert)");
            System.out.println(Arrays.toString(names));

            Arrays.sort(names);
            System.out.println("Namen (sortiert)");
            System.out.println(Arrays.toString(names));


            //Gesamt Ausgabe
            System.out.println("Name=ID:");
            System.out.println(person_data);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
