package prog2.tiefensuche;

import java.util.ArrayList;

public class Node {
    ArrayList<Node> neighbors = new ArrayList<>();
    String value;
    int dt;
    int ft;
    String mark;

    public Node(String value) {
        this.neighbors = neighbors;
        this.value = value;
    }

    @Override
    public String toString() {
        //return "Node with value: " + value + ":" + "\nDT: " + dt + "\nFT: " + ft  + "\nMark: " + mark + "\n";
        return "Node with value: " + value + ":" + "\nNachbar: " + neighbors;
    }

    public int asInt() {
        return (int) value.charAt(0);
    }


}
