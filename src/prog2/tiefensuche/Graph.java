package prog2.tiefensuche;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public class Graph {
    Stack<Node> stack = new Stack<>();
    ArrayList<Node> nodes = new ArrayList<>();


    public void init() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("graph.csv"));
        ArrayList<Node> temp_nodes = new ArrayList<>();
        String line = "";

        while ((line = br.readLine()) != null) {
            String[] data = line.split(",");
            Node n = new Node(data[0]);
            for (int i = 1; i < data.length; i++) {
                n.neighbors.add(new Node(data[i]));
            }
            nodes.add(n);
        }
    }







    Node findStart() {
        Node first = nodes.get(0);
        for (Node n: nodes) {
            if (first.asInt() > n.asInt()) {
                first = n;
            }
        }
        return first;
    }


    public static void main(String[] args) {
        Graph g = new Graph();
        try {
            g.init();
            System.out.println(g.nodes);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
