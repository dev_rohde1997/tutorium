package prog2.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuiMain {
    public static void main(String[] args) {
        JFrame m_frame = new JFrame();
        m_frame.setSize(500, 500);
        m_frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        m_frame.setLayout(new BorderLayout());

        JLabel label = new JLabel();
        label.setText("Das ist ein Label");
        //m_frame.add(label);

        JTextField jTextField = new JTextField();
        jTextField.setText("Hier eingeben");
        jTextField.setSize(50,50);
        m_frame.add(jTextField);

        m_frame.setVisible(true);

    }


    static JButton createButton(String text) {

        JButton m_button = new JButton();
        m_button.setText(text);
        m_button.setSize(100, 50);
        m_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showInputDialog(null, "Geben sie ihr Passwort ein:");
            }
        });
        return m_button;
    }

    static JTextField createjTF(String text) {

        JTextField textField = new JTextField();
        textField.setText(text);
        textField.setSize(50, 50);

        return textField;
    }
}


