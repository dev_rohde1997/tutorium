package prog2.threads;

public class MyThread  {
    public static void main(String[] args) {
        MeinThread t1 = new MeinThread("t1");
        MeinThread t2 = new MeinThread("t2");

        t1.start();
        try {
            t1.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
    }
}

class MeinThread extends Thread {
    String name;

    MeinThread(String name) {
        this.name = name;
    }
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(name + ": " + i);
        }
    }
}
