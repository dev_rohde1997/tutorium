package prog2.threads;

public class SyncMain {



}

class Document {
    private String text = "";
    private String author;
    //Lock one document
    private boolean lock = false;

    //Lock all documents
    public static boolean full_lock = false;


    public Document(String text, String author) {
        this.text = text;
        this.author = author;
    }

    public void changeDoc(String new_text) {
        if (Document.full_lock) return;

        if (lock) {
            System.out.println("Warten bis zur Freigabe");
        }
        synchronized (this) {
            this.lock = true;
            System.out.println("Gesichterter Block");
            //do stuff
            text += "\n" + new_text;
            //do stuff
        }
        this.lock = false;
        System.out.println("Document ist freigegeben");
    }

    private void securityBreachLock()  {
        Document.full_lock = true;
    }
}
