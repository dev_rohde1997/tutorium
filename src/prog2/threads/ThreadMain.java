package prog2.threads;

public class ThreadMain {

    public static void main(String[] args) {

        Zugriff zugriff = new Zugriff("Moritz", "root");

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Start t1");
                    zugriff.changePwd("eins");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Start t2");
                    zugriff.changePwd("zwei");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        t1.start();
        t2.start();




    }


}

class Zugriff {
    private String user;
    private String passwort;

    public Zugriff(String user, String passwort) {
        this.user = user;
        this.passwort = passwort;
    }

    public synchronized void changePwd(String passwort) throws InterruptedException{
        Thread.sleep(10500);
        System.out.println(this.passwort);
        this.passwort = passwort;
        System.out.println("Geändert: " + this.passwort);
    }

}
