package prog2.ttt.src;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TTTField extends Component {
	//Deklarieren und Init
	private JButton[][] btnGroup = new JButton[3][3];
	private JButton field00= new JButton();
	private JButton field01 = new JButton();
	private JButton field02 = new JButton();
	private JButton field10 = new JButton();
	private JButton field11 = new JButton();
	private JButton field12 = new JButton();
	private JButton field20 = new JButton();
	private JButton field21 = new JButton();
	private JButton field22 = new JButton();
	
	//Das Array mit den Buttons fuellen
	
	
	/**Konstruktor*/
	public TTTField() {
		super();
		this.fillField();
		
	}
	
	public void setButtonLocation(int x, int y) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (btnGroup[i][j] != null) {
					btnGroup[i][j].setBounds(x, y, 80, 50);
								
				}
			}
		}
		field00.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				field00.setText("X");
			}
			
			
		});
				
	}
	
	private void fillField() {
		btnGroup[0][0] = field00;
		btnGroup[0][1] = field01;
		btnGroup[0][2] = field02;
		btnGroup[1][0] = field10;
		btnGroup[1][1] = field11;
		btnGroup[1][2] = field12;
		btnGroup[2][0] = field20;
		btnGroup[2][1] = field21;
		btnGroup[2][2] = field22;
		}
			
	public JButton getButton(int row, int column) {
		return btnGroup[row][column];
	}
	
	

}
