package prog2.ttt.src;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 
 * @author Moritz Rohde 4677828 Gruppe 11c 
 *
 * @author Jan Philipp Althoefer 4700504 Gruppe 11c 
 * 
 * @author Felix Neumann 4708878 Gruppe 11c 
 * 
 * TicTacToe Spiel mit einer Graphischen Oberflaeche
 * 
 */
public class TicTacToo {

	
	//Deklarieren und Init
	/** Reset Button*/
	private JButton jBtnreset = new JButton();
	//TTTField field = new TTTField(50, 50);
	/**Beschriftung*/
	private JTextField jTFNameOne = new JTextField();
	/**Beschriftung*/
	private JTextField jTFNameTwo = new JTextField();
	/**Anzeige Name von Player 1*/
	private JTextField jTFNamePlayer1 = new JTextField();
	/**Anzeige Name von Player 2*/
	private JTextField jTFNamePlayer2 = new JTextField();
	/**Anzeige Name von Player 1 in der Punktetabelle*/
	private JTextField jTFPunktTabName1 = new JTextField();
	/**Anzeige Name von Player 2 in der Punktetabelle*/
	private JTextField jTFPunktTabName2 = new JTextField();
	/**Ueberschrift der Tabelle: Punkte*/
	private JTextField jTFPunkte = new JTextField();
	/**Punkte von Player1*/
	private JTextField punktePlayer1Field = new JTextField();
	/**Punkte von Player2*/
	private JTextField punktePlayer2Field = new JTextField();
	/**Name von Spieler 1*/
	private String name1 = JOptionPane.showInputDialog("Geben sie den Name von Spieler 1 ein");
	/**Name von Spieler 2*/
	private String name2 = JOptionPane.showInputDialog("Geben sie den Name von Spieler 2 ein");
	/**Punkte von Player 1*/
	private int punktePlayer1 = 0;
	/**Punkte von Player 2*/
	private int punktePlayer2 = 0;
	/**Das Fenster*/
	private JFrame f = new JFrame();
	/**TicTacTooField*/

	/** Button fuer TicTacToe-Feld */
	private JButton field00 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field01 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field02 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field10 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field11 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field12 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field20 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field21 = new JButton();
	/** Button fuer TicTacToe-Feld */
	private JButton field22 = new JButton();
	/** Buttongruppe fuer Zugriff auf alle Buttons des TicTacToe-Feldes */
	private JButton[][] btnGroup = new JButton[3][3];
	/** Variable fuer den aktuellen Spieler
	  * 0 keine Zuweisung
	  * 1 Spieler1
	  * 2 Spieler2
	  */
	private int beginner = 0;
	
	/** Diese Methode baut die Oberfläche fuer das Spielfeld auf */
	public void built() {
		this.fillField();
		System.out.println("Start");
		
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLayout(null);
		f.setLocation(700, 200);
		f.setSize(500, 500);
		f.setTitle("TicTacToo");
		f.getContentPane().setBackground(Color.MAGENTA);
		//Nimbus Style^^
		/*try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		} */
		f.setVisible(true);
		
		jTFNameOne.setBounds(10, 50, 50, 25);
		jTFNameOne.setText("Player 1:");
		jTFNameOne.setEnabled(false);
		f.getContentPane().add(jTFNameOne);
		
		jTFNameTwo.setBounds(10, 75, 50, 25);
		jTFNameTwo.setText("Player 2:");
		jTFNameTwo.setEnabled(false);
		f.add(jTFNameTwo);
		
		jTFNamePlayer1.setBounds(60, 50, 100, 25);
		jTFNamePlayer1.setText(name1);
		jTFNamePlayer1.setEnabled(false);
		f.add(jTFNamePlayer1);
		
		jTFNamePlayer2.setBounds(60, 75, 100, 25);
		jTFNamePlayer2.setText(name2);
		jTFNamePlayer2.setEnabled(false);
		f.add(jTFNamePlayer2);
		
		jTFPunktTabName1.setBounds(300, 100, 75, 25);
		jTFPunktTabName1.setText(name1);
		jTFPunktTabName1.setEnabled(false);
		f.add(jTFPunktTabName1);
		
		jTFPunktTabName2.setBounds(375, 100, 75, 25);
		jTFPunktTabName2.setText(name2);
		jTFPunktTabName2.setEnabled(false);
		f.add(jTFPunktTabName2);
		
		jBtnreset.setText("Reset");
		jBtnreset.setSize(50, 10);
		jBtnreset.setBounds(325, 200, 100, 25);
		jBtnreset.addActionListener(new ActionListener() {
			
			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				punktePlayer1 = 0;
				punktePlayer1Field.setText(String.valueOf(punktePlayer1));
				punktePlayer2 = 0;
				punktePlayer2Field.setText(String.valueOf(punktePlayer2));
				
				field00.setText("");
				field01.setText("");
				field02.setText("");
				field10.setText("");
				field11.setText("");
				field12.setText("");
				field20.setText("");
				field21.setText("");
				field22.setText("");
				//f.repaint();		
			}
		});
	
		f.add(jBtnreset);
		
		jTFPunkte.setText("Punkte");
		jTFPunkte.setBounds(325, 50, 100, 40); 
		jTFPunkte.setFont(new Font("My Font", 3, 20));
		jTFPunkte.setEnabled(false);
		f.add(jTFPunkte);
		
	
		punktePlayer1Field.setText(String.valueOf(punktePlayer1));
		punktePlayer1Field.setBounds(300, 150, 75, 25);
		punktePlayer1Field.setEnabled(false);
		f.add(punktePlayer1Field);
		
		punktePlayer2Field.setText(String.valueOf(punktePlayer2));
		punktePlayer2Field.setBounds(375, 150, 75, 25);
		punktePlayer2Field.setEnabled(false);
		f.add(punktePlayer2Field);
		
		int x = 25;
		int y;
		for (int i = 0; i < 3; i++) {
			y = 110;
			if (i == 0) {
				x = 25;
			} else {
				x += 80;
			}
			for (int j = 0; j < 3; j++) {
				y += 80;
				this.getButton(i, j).setBounds(x, y, 80, 80);
				this.getButton(i, j).setFont(new Font("My Font", 10, 50));
				this.getButton(i, j).setText("");
				f.add(this.getButton(i, j));
			}
		}
		
		field00.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field00.getText().equals("")) {
					if (beginner == 1) {
						field00.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field00.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});
		
		field01.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field01.getText().equals("")) {
					if (beginner == 1) {
						field01.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field01.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});

		field02.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field02.getText().equals("")) {
					if (beginner == 1) {
						field02.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field02.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}

			
		});

		field10.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field10.getText().equals("")) {
					if (beginner == 1) {
						field10.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field10.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});

		field11.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field11.getText().equals("")) {
					if (beginner == 1) {
						field11.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field11.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});

		field12.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field12.getText().equals("")) {
					if (beginner == 1) {
						field12.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field12.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});

		field20.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field20.getText().equals("")) {
					if (beginner == 1) {
						field20.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field20.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});

		field21.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field21.getText().equals("")) {
					if (beginner == 1) {
						field21.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field21.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});

		field22.addActionListener(new ActionListener() {

			/** ActionListener */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (field22.getText().equals("")) {
					if (beginner == 1) {
						field22.setText("X");
						beginner = 2;
					} else if (beginner == 2) {
						field22.setText("O");
						beginner = 1;
					} 
				}
				
				//f.repaint();
			}
			
		});
		
		
		
		f.repaint();
		
	}


	/** Methode, die das Spiel initialisiert
	  */ 
	public void play() {
		boolean won = false;
		
		if (beginner == 0) {
			
			beginner = Integer.parseInt(JOptionPane.showInputDialog(
						"Welcher Spieler beginnt? [1,2] "));
		} else if (beginner != 0 && beginner != 1 && beginner != 2) {
			System.out.println(beginner);
			JOptionPane.showMessageDialog(null, "Falsche Eingabe!");
			System.exit(-1);
				
		}
		while (won == false) {
			
			if (beginner == 1) {
				jTFPunktTabName1.setBackground(Color.red);
				jTFPunktTabName2.setBackground(Color.white);
			} else if (beginner == 2) {
				jTFPunktTabName1.setBackground(Color.white);
				jTFPunktTabName2.setBackground(Color.red);
			}
			
			//Game
			if (this.testWin().equals("Player1")) {
				punktePlayer1++;
				punktePlayer1Field.setText(String.valueOf(punktePlayer1));
				won = true;
			} else if (this.testWin().equals("Player2")) {
				punktePlayer2++;
				punktePlayer2Field.setText(String.valueOf(punktePlayer2));
				won = true;
			} else if (this.draw()) {
				won = true;
			}
			//f.repaint();
		}
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				this.getButton(i, j).setText("");
			}
		}
		//Naechste Runde
		
		this.play();
	}
	
	/** Methode, die testet, ob einer der beiden Spieler gewonnen hat
	  * @return Gewinner als String
	  */
	private String testWin() {
		if (this.winHorizontal("X") || this.winVertikal("X") || this.winDigonal("X")) {
			return "Player1";
		}
		
		if (this.winHorizontal("O") || this.winVertikal("O") || this.winDigonal("O")) {
			return "Player2";
		}
		return "NoWinner";
		
		
	}
	
	
	/** Methode die dem Feld Buttons zuweist
	  */
	private void fillField() {
		btnGroup[0][0] = field00;
		btnGroup[0][1] = field01;
		btnGroup[0][2] = field02;
		btnGroup[1][0] = field10;
		btnGroup[1][1] = field11;
		btnGroup[1][2] = field12;
		btnGroup[2][0] = field20;
		btnGroup[2][1] = field21;
		btnGroup[2][2] = field22;
	}
	
	/** Mathode die auf alle Buttons des TicTacToe-Feldes zugreift
	  * @param row Reihe vom TTT Feld
	  * @param column Spalte vom TTT Feld
	  * @return Button row/column
	  */
	private JButton getButton(int row, int column) {
		return btnGroup[row][column];
	}


	
	/** Methode, die prueft, ob ueber die Horizontale gewonnen wurde
	  * @param s "X" oder "O"
	  * @return true bei Sieg ueber Horizontale
	  */
	private boolean winHorizontal(String s) {
		for ( int i = 0; i < 1; i++) {
			for ( int j = 0; j < 3; j++) {
				if (this.getButton(i, j).getText().equals(s)
					   && this.getButton(i + 1, j).getText().equals(s)
					   && this.getButton(i + 2, j).getText().equals(s)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/** Methode, die prueft, ob ueber die Vertikale gewonnen wurde
	  * @param s "X" oder "O"
	  * @return true bei Sieg ueber Vertikale
	  */
	private boolean winVertikal(String s) {
		for ( int i = 0; i < 3; i++) {
			for ( int j = 0; j < 1; j++) {
				if (this.getButton(i, j).getText().equals(s)
					   && this.getButton(i, j + 1).getText().equals(s)
					   && this.getButton(i, j + 2).getText().equals(s)) {
					return true;
				}
			}
		}
		return false;
	}

	/** Methode, die prueft, ob ueber die Diagonale gewonnen wurde
	  * @param s "X" oder "O"
	  * @return true bei Sieg ueber Diagonale
	  */
	private boolean winDigonal(String s) {
		if (this.getButton(0, 0).getText().equals(s)
			   && this.getButton(1, 1).getText().equals(s)
			   && this.getButton(2, 2).getText().equals(s)) {
			return true;
		}
		if (this.getButton(0, 2).getText().equals(s)
			   && this.getButton(1, 1).getText().equals(s)
			   && this.getButton(2, 0).getText().equals(s)) {
			return true;
		}
		return false;
	}

	/** Methode, die prueft ob alle Felder belegt sind,
	  * um unentschieden zu pruefen
	  * @return true bei Untentschieden
	  */
	private boolean draw() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				//System.out.println("Button" + i + j + this.getButton(i, j).getText().equals(""));
				if (this.getButton(i, j).getText().equals("")) {
					return false;
				}
			}
		}
		return true;
	}

	public JFrame getF() {
		return f;
	}
}
