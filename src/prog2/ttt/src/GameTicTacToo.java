package prog2.ttt.src;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * 
 * @author Moritz Rohde 4677828 Gruppe 11c 
 *
 * @author Jan Philipp Althoefer 4700504 Gruppe 11c 
 * 
 * @author Felix Neumann 4708878 Gruppe 11c 
 * 
 * TestKlasse um ein TicTacToe-Spiel zu starten.
 * 
 */
public class GameTicTacToo {

	/** GuiMain Methode
	 * 
	 * @param args Argument
	 */
	public static void main(String[] args) {
		TicTacToo game = new TicTacToo();
		game.built();
		game.play();
		ChangeColorThread thread = new ChangeColorThread(game.getF());
		thread.start();

	}

	
	
}

class ChangeColorThread extends Thread{
	JFrame f;

	public ChangeColorThread(JFrame f) {
		this.f = f;
	}

	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(5);
				// Java 'Color' class takes 3 floats, from 0 to 1.
				Random rand = new Random();
				float r = rand.nextFloat();
				float g = rand.nextFloat();
				float b = rand.nextFloat();
				Color randomColor = new Color(r, g, b);
				f.getContentPane().setBackground(randomColor);
				f.repaint();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}


		}

	}
}
