package prog2.binary_tree;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Binary_Tree {
    Node root = new Node();


    public static void main(String[] args) {
        try {
            //Testen

            //Read file
            Scanner scanner = new Scanner(Paths.get("OK.txt"));
            Binary_Tree binary_tree = new Binary_Tree();

            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                System.out.println(line);
                int value = Integer.parseInt(line);
                binary_tree.insert(new Node(value));

            }


            System.out.println(binary_tree.root);
            System.out.println(binary_tree.root.contains(50));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void insert(Node n) {
        root.insert(n);
    }

    public boolean contains(Integer i) {
        return root.contains(i);
    }

    @Override
    public String toString() {
        return root.toString();
    }
}
