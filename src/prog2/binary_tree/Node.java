package prog2.binary_tree;

import org.jetbrains.annotations.NotNull;

public class Node<T extends Comparable<T>> implements Comparable<T>{
    T value = null;
    Node right;
    Node left;

    public Node(T value) {
        this.value = value;
        left = new Node();
        right = new Node();
    }

    //Required empty constructor
    public Node() {
    }

    @Override
    public int compareTo(@NotNull T o) {
        return value.compareTo(o);
    }

    @Override
    public String toString() {
        if (value == null) return "";
        if (left == null && right == null) return value + ",";
        if (left == null) return "" + value + "," + right;
        if (right == null) return "" + left + value +",";
        return "" + left + value + "," + right;
    }

    public void insert(Node<T> n) {
        if (n == null) return;
        if (value == null) {
            value = n.value;
            return;
        } else if (n.compareTo(value) <= 0){
            if (left == null)
                left = n;
            else
                left.insert(n);
        } else {
            if (right == null)
                right = n;
            else
                right.insert(n);
        }
    }

    public boolean contains(T value) {
        if (this.value == null || value == null) return false;
        if (this.value == value) return true;
        if (this.value.compareTo(value) >= 0) {
            if (left == null) {
                return false;
            }
            return left.contains(value);
        } else {
            if (right == null) {
                return false;
            }
            return right.contains(value);
        }
    }

}
