package prog2.binary_tree;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Test_Tree {

    @Test
    public void test_toString() {
        Binary_Tree binary_tree = initTree();
        assertEquals("0,1,2,3,4,5,6,7,8,9,", binary_tree.toString());
    }

    @Test
    public void test_insert_1() {
        Binary_Tree binary_tree = initTree();
        binary_tree.insert(new Node(11));
        assertTrue(binary_tree.contains(11));
    }

    @Test
    public void test_insert_2() {
        Binary_Tree binary_tree = initTree();
        binary_tree.insert(null);
        assertEquals("0,1,2,3,4,5,6,7,8,9,", binary_tree.toString());

    }

    @Test
    public void contains_test_1() {
        Binary_Tree binary_tree = initTree();
        assertTrue(binary_tree.contains(4));
    }

    @Test
    public void contains_test_2() {
        Binary_Tree binary_tree = initTree();
        assertFalse(binary_tree.contains(-1));
    }

    @Test
    public void contains_test_3() {
        Binary_Tree binary_tree = initTree();
        assertFalse(binary_tree.contains(null));
    }


    private Binary_Tree initTree() {
        int tree_size = 10;
        Binary_Tree binary_tree = new Binary_Tree();
        for (int i = 0; i < tree_size; i++) {
            binary_tree.insert(new Node(i));
        }
        return binary_tree;
    }
}
