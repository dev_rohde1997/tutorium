package prog2.my_list;

public class Node {
    int item;
    Node next;


    public void insert(int x) {
        Node n = new Node();
        n.item = x;
        n.next = next;
        next = n;
    }

    public boolean contains(int i) {
        if (next == null) {
            return false;
        }

        if (next.item == i) {
            return true;
        } else {
            return next.contains(i);
        }
    }


    @Override
    public String toString() {
        if (next == null) {
            return "";
        }
        return " ->" + next.item + next;
    }


}


