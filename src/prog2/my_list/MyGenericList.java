package prog2.my_list;

import org.jetbrains.annotations.NotNull;

public class MyGenericList<N extends Comparable<N>> implements Comparable<N> {

    MyGenericList next = null;
    N value;


    MyGenericList(N i) {
        this.value = i;
    }

    MyGenericList() {

    }

    public boolean isEmpty() {
        return next == null;
    }

    public boolean isInList(N x) {
        if (isEmpty()) {
            return false;
        } else
        if (firstElement().compareTo(x) == 0)
            return true;
        else
            return next.isInList(x);
    }


    public N firstElement() {
        if (isEmpty()) return null;
        return (N) next.value;
    }


    public int length() {
        return next.length() + 1;
    }


    public MyGenericList sortedInsert(N x) {
        if (x == null) throw new IllegalArgumentException();

        MyGenericList l = new MyGenericList(x);

        if (next == null) {
            l.next = next;
            next = l;
            return this;
        } else if (next.value.compareTo(x) < 0) {
            return next.sortedInsert(x);
        } else {
            l.next = next;
            next = l;
            return this;
        }
    }


    public MyGenericList delete(N x) {
        if (next == null) {
            System.err.println("Delete(" + x + ") Not found!");
            return null;
        }
        if (next.value.compareTo(x) == 0) {
            next = next.next;
        } else {
            next.delete(x);
        }
        return this;
    }


    @Override
    public String toString() {
        if (isEmpty()) {
            return "";
        } else {
            return " -> " + next.value + next;
        }
    }

    @Override
    public int compareTo(@NotNull N o) {
        return value.compareTo(o);
    }
}

