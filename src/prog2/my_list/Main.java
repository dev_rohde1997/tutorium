package prog2.my_list;

public class Main {
    public static void main(String[] args) {

        Typ<Integer> t1 = new Typ<>();
        t1.value = 24;


        Typ<Integer> t2 = new Typ<>();
        t2.value = 24;

        System.out.println(t1.equals(t2));
    }
}

class Typ<T> {
    T value;

    @Override
    public boolean equals(Object obj) {
        Typ temp = (Typ) obj;
        return this.value.equals(temp.value);
    }
}
