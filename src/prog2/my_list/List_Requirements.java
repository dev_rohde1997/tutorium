package prog2.my_list;

public interface List_Requirements {
    boolean isEmpty();
    boolean isInList(int x);
    int firstElement();
    int length();
    MyList insert(int x);
    MyList append(int x);
    MyList delete(int x);
    MyList delete();
}
