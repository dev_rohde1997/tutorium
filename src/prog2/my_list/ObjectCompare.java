package prog2.my_list;

public class ObjectCompare<T> {
    public static void main(String[] args) {
        typAusgabe(new Klasse<Integer>());


    }

    public static <T> void typAusgabe(T t) {
        if (t instanceof Comparable) {
            System.out.println("Ich bin ein stolzer Integer");
        } else {
            System.out.println("Ich bin ein " + t.getClass().getName());
        }
    }
}


class Klasse<T extends Comparable<T> > {
    private T value;

    public static <T> void typAusgabe(T t) {
        if (t instanceof Integer) {

        }
    }

}


