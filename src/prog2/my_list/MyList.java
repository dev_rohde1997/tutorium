package prog2.my_list;

public class MyList implements List_Requirements {

    MyList next = null;
    int value;


    MyList(int i) {
        this.value = i;
    }

    MyList() {

    }



    @Override
    public boolean isEmpty() {
        return next == null;
    }

    @Override
    public boolean isInList(int x) {
        if (isEmpty()) {
            return false;
        } else
        if (firstElement() == x)
            return true;
        else
            return next.isInList(x);
    }

    @Override
    public int firstElement() {
        if (isEmpty()) return -1;
        return next.value;
    }

    @Override
    public int length() {
        return next.length() + 1;
    }

    @Override
    public MyList insert(int x) {
        MyList l = new MyList(x);
        l.next = next;
        next = l;
        return this;
    }


    @Override
    public MyList append(int x) {
        if (isEmpty()) {
            return insert(x);
        } else {
            return next.append(x);
        }
    }

    @Override
    public MyList delete(int x) {
        if (next == null)
            return null;
        if (next.value == x) {
            next = next.next;
        } else {
            next.delete(x);
        }
        return this;
    }

    @Override
    public MyList delete() {
        next = null;
        return this;
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "";
        } else {
            return " -> " + next.value + next;
        }
    }
}

