package prog2.bubble_recursive;

/**
 * @author Moritz Rohde
 * Das Programm dient zur Ausfürung und dem Testen der Klassen
 * NumberFileReader
 * und
 * Bubblesort
 */

public class Sorting_Test {

    public static void main(String[] args) {
        String path = "/Users/moritz/IdeaProjects/tutorium_programmieren/src/prog2/bubble_recursive/numbers.txt";

        //Create NumberReader class
        NumberFileReader numberFileReader = new NumberFileReader(path);
        //Read numbers
        int[] numbers = numberFileReader.readNumberList(",");

        //Create sorting class
        Bubblesort bubblesort = new Bubblesort(numbers);

        //Print raw numbers
        bubblesort.print(", ");

        //Sorting
        bubblesort.bubbleSort();

        //Output
        bubblesort.print(", ");
    }
}
