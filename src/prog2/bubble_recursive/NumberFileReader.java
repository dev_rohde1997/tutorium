package prog2.bubble_recursive;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * @author Moritz Rohde
 * Das Programm liest auf einer Datei alle Daten
 * Es gibt eine spezielle Methode, die eine Umwandlung in eine Liste von Zahlen bietet
 */

public class NumberFileReader {

    private String path;

    /**
     * @param path Dateipfad
     */
    public NumberFileReader(String path) {
        if (path == null | path.equals("")) {
            throw new IllegalArgumentException();
        }
        this.path = path;
    }

    String line = null;

    /**
     * Methode zum Lesen aus einer Datei.
     *
     * @return Gelesene Daten
     */
    public String getRawData() {
        try {
            String data = "";
            String line = null;
            BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(this.path));

            while ((line = bufferedReader.readLine()) != null) {
                data += line;
            }
            return data;
        } catch (IOException io) {
            System.err.println("Error during loading data");
            io.printStackTrace();
        }
        return null;
    }

    /**
     * Methode zum Lesen einer Zahlenfolge aus einer Datei
     *
     * @param separator Zeichen durch die die Zahlen in der Datei getrennt sind
     * @return Zahlenliste
     */
    public int[] readNumberList(String separator) {
        String raw_data = this.getRawData();
        String[] seperated_data = raw_data.split(separator);
        int[] int_list = this.conv_StringList_to_IntList(seperated_data);
        if (int_list == null) throw new IllegalArgumentException("Corrupt data");
        return int_list;

    }

    /**
     * Convertiert eine String Liste zu einer Integer Liste
     *
     * @param list Liste mit Strings
     * @return Liste mit Integern
     */
    private int[] conv_StringList_to_IntList(String[] list) {
        if (list.length == 0) return null;
        int[] int_list = new int[list.length];
        int i = 0;
        for (String s : list) {
            if (s == null) return null;
            int_list[i] = Integer.parseInt(s);
            i++;
        }
        return int_list;
    }
}
