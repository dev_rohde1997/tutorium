package prog2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

public class Clone {
    public static void main(String... args) throws CloneNotSupportedException{
        MyTest test = new MyTest() {
            @Override
            public String toString() {
                return "Clone";
            }
        };

        MyTest test2 = (MyTest) test.clone();

    }
}

class MyTest {
    @Override
    protected Object clone() throws CloneNotSupportedException {
        MyTest test = new MyTest();
        return test;
    }
}
