package prog2.ha03;

import java.util.ArrayList;
import java.util.Stack;

public class Graph {

    ArrayList<Node> node_liste = new ArrayList<>();
    boolean connected = false;
    //ArrayList<Node> kanten_liste = new ArrayList<>();

    public static void main(String[] args) {
        Graph g = new Graph();
        Node n1 = new Node("A");
        Node n2 = new Node("B");
        Node n3 = new Node("C");
        Node n4 = new Node("D");
        Node n5 = new Node("E");

        g.node_liste.add(n1);
        g.node_liste.add(n2);
        g.node_liste.add(n3);
        g.node_liste.add(n4);
        g.node_liste.add(n5);

        g.addEdge(n1, n2);
        g.addEdge(n1, n5);
        g.addEdge(n2, n5);
        g.addEdge(n3, n5);

        //System.out.println(g);

        g.checkConnectivity();
    }




    @Override
    public String toString() {
        String out = "";
        for (Node n : node_liste) {
            out += n.toString();
        }
        return out;
    }

    void addNode(Node k) {
        if (name_exists(k) || node_liste.contains(k)) {
            System.out.println("Node existiert schon");
            return;
        }


        node_liste.add(k);
    }

    void addEdge(Node node1, Node node2) {

        if(node1.equals(node2)) {
            System.out.println("Keine zirkulären Kanten");
            return;
        }
        //Überprüfen, ob es Kante schon gibt
        for (Edge e : node1.getEdge()) {
            //System.out.println(e);
            if ((e.getNode1().equals(node1) && e.getNode2().equals(node2)) ||
                (e.getNode2().equals(node1) && e.getNode1().equals(node1))) {
                System.out.println("Kante gibt es schon");
                return;
            }

        }

        if (!node_liste.contains(node1)) {
            System.out.println("Node1 ist nicht im Graphen");
            return;
        }

        if (!node_liste.contains(node2)) {
            System.out.println("Node2 ist nicht im Graphen");
            return;
        }

        //Neue Edge
        Edge e = new Edge(node1, node2, 1);
        node1.getEdge().add(e);
        node2.getEdge().add(e);
    }

    void removeEdge(Edge e) {
        Node ziel;
        for (Node k : node_liste) {
            for (Edge ed : k.getEdge()) {
                if (ed.equals(e)) {
                    //Edge gefunden
                    ed.getNode1().getEdge().remove(e);
                    ed.getNode2().getEdge().remove(e);
                }
            }
        }


        //Edge nicht gefunden
    }

    boolean checkConnectivity() {

        ArrayList<Node> nodes = new ArrayList<>(node_liste);
        ArrayList<Node> besucht = new ArrayList<>();
        ArrayList<Node> finished = new ArrayList<>();
        Stack<Node> deepsearch = new Stack<>();


        //Start this one node
        if (nodes.size() == 0) return false;
        Node start = nodes.get(0);
        deepsearch.push(start);
        dfs(deepsearch, besucht);

        System.out.print("Der Graph ist ");
        if (connected) {
            System.out.println("zusammenhängend");
            return true;
        } else {
            System.out.println("nicht zusammenhängend");
            return false;
        }
    }

    private void dfs(Stack<Node> deepstack, ArrayList<Node> besucht) {

        System.out.println(deepstack.peek().getName());
        besucht.add(deepstack.peek());
        ArrayList<Node> unbesuchteNachbarn = new ArrayList<>();
        for (Edge e : deepstack.peek().getEdge()) {
            //System.out.println(e);
            Node nachbar = deepstack.peek().getNeightbor(e);
            if (!besucht.contains(nachbar)) {
                //System.out.println(nachbar.getName() + " ist unbesucht.");
                unbesuchteNachbarn.add(nachbar);
            }
        }

        if (unbesuchteNachbarn.isEmpty()) {
            //System.out.println("POP:" + deepstack.peek().getName());
            deepstack.pop();
        } else {
            for (Node n : unbesuchteNachbarn) {
                deepstack.push(n);
                dfs(deepstack, besucht);
            }
            //System.out.println("POP:" + deepstack.peek().getName());
            deepstack.pop();

            if (deepstack.isEmpty()) {
                compareResult(besucht);
            }
        }
    }

    private void compareResult(ArrayList<Node> besucht) {
        //Überprüfe manuell Listen
                /*for (Node n: besucht) {
                    System.out.print(n.getName() + ", ");
                }
                System.out.println();

                for (Node n : node_liste) {
                    System.out.print(n.getName() + ", ");
                }
                System.out.println();*/
        //DFS Abgeschlossen
        //Vergleiche besucht mit Knoten litse


        for (Node n : node_liste) {
            if (!besucht.contains(n)) {
                //Wenn ein Knoten nicht besucht ist, dann ist der Graph nicht zusammenhängend
                connected = false;
                return;
            }
        }
        connected = true;
    }


     void removeNode(Node n) {
        for (Edge e : n.getEdge()) {
            Node temp;
            if (e.getNode1().equals(n))
                temp = e.getNode2();
            else
                temp = e.getNode1();
            temp.getEdge().remove(e);
        }
        node_liste.remove(n);
    }


    private boolean name_exists(Node k_new) {
        for (Node k : node_liste) {
            System.out.println(k.getName());
            if (k.getName().equals(k_new.getName())) {
                return true;
            }
        }
        return false;
    }

}


class Node {
    private ArrayList<Edge> edge = new ArrayList<>();
    private String name;

    Node(String name) {
        this.name = name;
    }

    public ArrayList<Edge> getEdge() {
        return edge;
    }


    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        String out = "";

        for (Edge e : edge) {
            out += getName() + ":\n\t"+ e + "\n";
        }

        return out;
    }

    Node getNeightbor(Edge e) {
        if (e.getNode1() == this)
            return e.getNode2();
        else
            return e.getNode1();
    }

    @Override
    public boolean equals(Object obj) {
        Node n = (Node) obj;
        return n.getName().equals(getName());
    }
}

class Edge {
    private Node node1;
    private Node node2;
    private int gewicht;

    public Edge(Node k1, Node k2, int gewicht) {
        this.node1 = k1;
        this.node2 = k2;
        this.gewicht =gewicht;
    }


    public void setNode1(Node node1) {
        this.node1 = node1;
    }

    public Node getNode2() {
        return node2;
    }

    public void setNode2(Node node2) {
        this.node2 = node2;
    }

    public int getGewicht() {
        return gewicht;
    }

    public void setGewicht(int gewicht) {
        this.gewicht = gewicht;
    }

    @Override
    public boolean equals(Object obj) {
        Edge e = (Edge) obj;
        return e.getNode1().equals(node1) && e.getNode2().equals(node2) ||
            e.getNode2().equals(node1) && e.getNode1().equals(node1);
    }

    public Node getNode1() {
        return node1;
    }

    @Override
    public String toString() {
        return "" + node1.getName() + " - " + node2.getName() + "[" + getGewicht() + "]";
    }
}